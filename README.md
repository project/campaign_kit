Description
-----------
Campaign Kit allows you to create custom campaigns within your Drupal site (version 8 or higher). 
Though there are many third-party services that perform similar functions, only when the code is running
on your site can you add custom functionality and a theme that exactly matches the rest of your site. 

One way to extend Campaign Kit is via event subscribers when certain actions are triggered. Please see
the documentation for instructions.

Documentation
-------------
Find the documentation at:
https://performantlabs.gitbook.io/campaign-kit

Installation
------------
To install this module, you will require [Composer](https://getcomposer.org) on your system. See the instructions
at [Using Composer with Drupal](https://www.drupal.org/docs/develop/using-composer/using-composer-with-drupal).

On the command line, instruct Composer to add the module to your project:

```cd <project directory>```

```composer require drupal/campaign_kit```

Or, add the following to your project composer.json file with a text editor:

```"drupal/campaign_kit": "^1.0"```

Then run:
```composer update```

Composer will install all libraries and modules on which Campaign Kit depends. Installing the module without Composer 
is not recommended and is unsupported.

Enabling the Module
-------------------
Enable the module by choosing it from /admin/modules or use Drush:

```cd <project directory/web>```

```drush en -y campaign_kit```

Or, to work with some example data:
```drush en -y campaign_kit_demo```

Allow Drupal to enable all the dependent modules when you are asked.
