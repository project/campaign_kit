<?php

namespace Drupal\campaign_kit\Campaign;

use Drupal\campaign_kit_core\Entity\CampaignDonation;
use Drupal\campaign_kit_core\Entity\CampaignTransaction;

/**
 * Provide extra functions for campaigns.
 *
 * @package Drupal\campaign_kit\Campaign
 */
class CampaignAPI {

  /**
   * Create Campaign function.
   *
   * @param array $values
   *   Array values to create a Campaign entity.
   *
   * @return string
   *   Returns a object.
   */
  public function createCampaign(array $values = []) {
    return 'createCampaign';
  }

  /**
   * Creates a Campaign Donation.
   *
   * @param array $values
   *   Values to create a Campaign Donation entity.
   *
   * @return \Drupal\campaign_kit_core\Entity\CampaignDonation
   *   Returns a Campaign Donation entity.
   */
  public function createCampaignDonation(array $values = []) {
    // Create Campaign Donation entity.
    /* @var $campaignDonation \Drupal\campaign_kit_core\Entity\CampaignDonation */
    $campaignDonation = CampaignDonation::create($values);

    return $campaignDonation;
  }

  /**
   * Creates a Campaign Transaction.
   *
   * @param array $values
   *   Values to create a Campaign entity.
   *
   * @return \Drupal\campaign_kit_core\Entity\CampaignTransaction
   *   Returns a Campaign Transaction.
   */
  public function createCampaignTransaction(array $values = []) {
    // Create Campaign Transaction entity.
    /* @var $campaignTransaction \Drupal\campaign_kit_core\Entity\CampaignTransaction */
    $campaignTransaction = CampaignTransaction::create($values);

    return $campaignTransaction;
  }

  /**
   * Add conditions to a view.
   *
   * @param $query
   * @param $fieldName
   * @param $lowestAmount
   * @param $highestAmount
   */
  public function alterExposedForm(&$query, $fieldName, $lowestAmount, $highestAmount){
    if (isset($lowestAmount) && $lowestAmount != 0) {
      $query->where[0]['conditions'][4]['field'] = $fieldName . '.amount';
      $query->where[0]['conditions'][4]['value'] = (int) $lowestAmount;
      $query->where[0]['conditions'][4]['operator'] = '>=';
    }

    if (isset($highestAmount) && $highestAmount != 0) {
      $query->where[0]['conditions'][5]['field'] = $fieldName . '.amount';
      $query->where[0]['conditions'][5]['value'] = (int) $highestAmount;
      $query->where[0]['conditions'][5]['operator'] = '<=';
    }
  }

  /**
   * Return United States' states in an array.
   *
   * @return array
   */
  public function getUSStates() {
    return $stateOptions = [
      'AL' => t('Alabama'),
      'AK' => t('Alaska'),
      'AZ' => t('Arizona'),
      'AR' => t('Arkansas'),
      'CA' => t('California'),
      'CO' => t('Colorado'),
      'CT' => t('Connecticut'),
      'DE' => t('Delaware'),
      'DC' => t('District of Columbia'),
      'FL' => t('Florida'),
      'GA' => t('Georgia'),
      'HI' => t('Hawaii'),
      'ID' => t('Idaho'),
      'IL' => t('Illinois'),
      'IN' => t('Indiana'),
      'IA' => t('Iowa'),
      'KS' => t('Kansas'),
      'KY' => t('Kentucky'),
      'LA' => t('Louisiana'),
      'ME' => t('Maine'),
      'MD' => t('Maryland'),
      'MA' => t('Massachusetts'),
      'MI' => t('Michigan'),
      'MN' => t('Minnesota'),
      'MS' => t('Mississippi'),
      'MO' => t('Missouri'),
      'MY' => t('Montana'),
      'NE' => t('Nebraska'),
      'NV' => t('Nevada'),
      'NH' => t('New Hampshire'),
      'NJ' => t('New Jersey'),
      'NM' => t('New Mexico'),
      'NY' => t('New York'),
      'NC' => t('North Carolina'),
      'ND' => t('North Dakota'),
      'OH' => t('Ohio'),
      'OK' => t('Oklahoma'),
      'OR' => t('Oregon'),
      'PA' => t('Pennsylvania'),
      'RI' => t('Rhode Island'),
      'SC' => t('South Carolina'),
      'SD' => t('South Dakota'),
      'TN' => t('Tennessee'),
      'TX' => t('Texas'),
      'UT' => t('Utah'),
      'VT' => t('Vermont'),
      'VA' => t('Virginia'),
      'WA' => t('Washington'),
      'WV' => t('West Virginia'),
      'WI' => t('Wisconsin'),
      'WY' => t('Wyoming'),
    ];
  }

  /**
   * Return Canada's states in an array.
   *
   * @return array
   */
  public function getCanadaStates() {
    return $stateOptions = [
      'AB' => t('Alberta'),
      'BC' => t('British Columbia'),
      'MB' => t('Manitoba'),
      'NB' => t('New Brunswick'),
      'NL' => t('Newfoundland and Labrador'),
      'NS' => t('Nova Scotia'),
      'ON' => t('Ontario'),
      'PE' => t('Prince Edward Island'),
      'QC' => t('Quebec'),
      'SK' => t('Saskatchewan'),
      'NT' => t('Northwest Territories'),
      'NU' => t('Nunavut'),
      'YT' => t('Yukon'),
    ];
  }

}
