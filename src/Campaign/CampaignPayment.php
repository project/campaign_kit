<?php

namespace Drupal\campaign_kit\Campaign;

use Drupal;

/**
 * Define CampaignPayment.
 *
 * @package Drupal\campaign_kit\Campaign
 */
class CampaignPayment {

  /**
   * Get Payment Method.
   *
   * @return string
   *   Returns payment method.
   */
  public function getPaymentMethod() {
    $paymentMethodManager = Drupal::service('plugin.manager.payment.method');
    $method = '';
    foreach ($paymentMethodManager->getDefinitions() as $pluginId => $definition) {
      if ($definition['active']) {
        $method = $pluginId;
      }
    }

    return $method;
  }

  /**
   * Get Payment Type.
   *
   * @return string
   *   Returns the payment type.
   */
  public function getPaymentType() {
    $paymentTypeManager = Drupal::service('plugin.manager.payment.type');
    $type = '';
    unset($paymentTypeManager->getDefinitions()['payment_unavailable']);
    foreach ($paymentTypeManager->getDefinitions() as $pluginId => $definition) {
      $type = $pluginId;
    }

    return $type;
  }

}
