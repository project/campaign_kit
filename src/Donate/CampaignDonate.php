<?php

namespace Drupal\campaign_kit\Donate;

use Stripe\Charge;
use Stripe\Stripe;

/**
 * The Campaign Donate entity.
 *
 * @package Drupal\campaign_kit\Donate
 */
class CampaignDonate {

  /**
   * Helper function for checking Stripe API Keys.
   *
   * @param string $config
   *   The config factory service.
   *
   * @return bool $status
   *   Boolean status, TRUE or FALSE.
   */
  public function checkTestStripeApiKey($config) {
    $status = FALSE;
    // Do not forget change this to live when it is in production.
    if ($config->get('environment') == 'test' && $config->get('apikey.test.secret')) {
      $status = TRUE;
    }
    return $status;
  }

  /**
   * Helper function for charge.
   *
   * @param string $config
   *   The config factory service.
   * @param string $token
   *   Stripe API token.
   * @param int $amount
   *   Amount for charge.
   * @param string $currency
   *   Code for currency.
   * @param string $campaignId
   *   Campaign id.
   * @param string $campaignTitle
   *   Campaign title.
   * @param string $email
   *   Email.
   *
   * @return \Stripe\Charge
   *   Charge object.
   */
  public function createCharge($config, $token, $amount, $currency, $campaignId, $campaignTitle, $email) {
    $apiKey = $config->get('apikey.test.secret');
    // Set your secret key: remember to change this to your live secret
    // key in production.
    // See your keys here: https://dashboard.stripe.com/account/apikeys
    Stripe::setApiKey($apiKey);

    $intAmount = (integer) $amount;
    // Charge the user's card:
    /* @var $charge \Stripe\Charge */
    $charge = Charge::create([
      "amount" => $intAmount * 100,
      "currency" => $currency,
      "description" => $campaignTitle,
      "source" => $token,
      "metadata" => [
        "order_id" => $campaignId,
        "campaign_id" => $campaignId,
        "campaign_label" => $campaignTitle,
      ],
      "receipt_email" => $email,
    ]);

    return $charge;
  }

}
