<?php

namespace Drupal\campaign_kit\EventSubscriber;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\core_event_dispatcher\Event\Core\CronEvent;
use Drupal\views_event_dispatcher\Event\Views\ViewsQueryAlterEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * CampaignKitEventSubscriber.
 *
 * @package Drupal\campaign_kit_demo
 */
class CampaignKitEventSubscriber implements EventSubscriberInterface {

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(LoggerChannelFactoryInterface $loggerFactory) {
    $this->loggerFactory = $loggerFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];

    // TODO: possibly use this in the future when the patch here gets in:
    // https://www.drupal.org/project/hook_event_dispatcher/issues/3016792
    // $events[HookEventDispatcherEvents::VIEWS_QUERY_ALTER][] = ['queryAlter'];.
    $events[HookEventDispatcherInterface::CRON][] = ['cronEvent'];
    return $events;
  }

  /**
   * Query alter event handler.
   *
   * @param \Drupal\hook_event_dispatcher\Event\Views\ViewsQueryAlterEvent $event
   *   The event.
   */
  public function queryAlter(ViewsQueryAlterEvent $event) {
    // TODO: Is this required?.
    $view = $event->getView();
    $query = $event->getQuery();

    if ($view->storage->get('id') === 'campaign_kit_supporters' && $view->current_display === 'block_2') {
      // Do something with the view or query.
    }
  }

  /**
   * Cron event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\Cron\CronEvent $event
   *   The event.
   */
  public function cronEvent(CronEvent $event) {
    $dispatcherType = $event->getDispatcherType();
    $this->loggerFactory->get('campaign_kit')->debug('Cron event');
    $this->loggerFactory->get('campaign_kit')->debug($dispatcherType);
  }

}
