<?php

namespace Drupal\campaign_kit\Controller;

use Drupal\campaign_kit_core\Entity\Campaign;
use Drupal\campaign_kit_core\Entity\Team;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityFormBuilderInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Link;
use Drupal\path_alias\AliasManager;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Routing\RouteProvider;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller for Campaign Kit.
 */
class CampaignKitController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity form builder.
   *
   * @var \Drupal\Core\Entity\EntityFormBuilderInterface
   */
  protected $entityFormBuilder;

  /**
   * The route provider.
   *
   * @var \Drupal\Core\Routing\RouteProvider
   */
  protected $routeProvider;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The path alias manager.
   *
   * @var \Drupal\Core\Path\AliasManager
   */
  protected $pathAliasManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * The request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The current active database's master connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * CampaignKitController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFormBuilderInterface $entityFormBuilder
   *   The entity form builder.
   * @param \Drupal\Core\Routing\RouteProvider $routeProvider
   *   The route provider.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Path\AliasManager $pathAliasManager
   *   The path alias manager.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Path\CurrentPathStack $currentPath
   *   The current path.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param \Drupal\Core\Database\Connection $connection
   *   The current active database's master connection.
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   The date formatter service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, EntityFormBuilderInterface $entityFormBuilder, RouteProvider $routeProvider, ModuleHandlerInterface $moduleHandler, AliasManager $pathAliasManager, ConfigFactory $configFactory, AccountProxyInterface $currentUser, CurrentPathStack $currentPath, Request $request, Connection $connection, DateFormatter $dateFormatter) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFormBuilder = $entityFormBuilder;
    $this->routeProvider = $routeProvider;
    $this->moduleHandler = $moduleHandler;
    $this->pathAliasManager = $pathAliasManager;
    $this->configFactory = $configFactory;
    $this->currentUser = $currentUser;
    $this->currentPath = $currentPath;
    $this->request = $request;
    $this->connection = $connection;
    $this->dateFormatter = $dateFormatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @var \Symfony\Component\DependencyInjection\ContainerInterface */
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity.form_builder'),
      $container->get('router.route_provider'),
      $container->get('module_handler'),
      $container->get('path_alias.manager'),
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('path.current'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('database'),
      $container->get('date.formatter')
    );
  }

  /**
   * Return campaign links for a menu.
   *
   * @return mixed
   *   Returns array of links.
   */
  public function campaignKitPage() {
    $links = [];

    // Campaign list.
    if (count($this->routeProvider->getRoutesByNames(['entity.campaign.collection'])) === 1) {
      $campaignList = Url::fromRoute('entity.campaign.collection');
      $links[] = Link::fromTextAndUrl($this->t('Campaigns'), $campaignList);
    }

    // Campaign Donation list.
    if (count($this->routeProvider->getRoutesByNames(['entity.campaign_donation.collection'])) === 1) {
      $campaignDonationList = Url::fromRoute('entity.campaign_donation.collection');
      $links[] = Link::fromTextAndUrl($this->t('Campaign donations'), $campaignDonationList);
    }

    // Campaign Update list.
    if (count($this->routeProvider->getRoutesByNames(['entity.campaign_update.collection'])) === 1) {
      $campaignUpdateList = Url::fromRoute('entity.campaign_update.collection');
      $links[] = Link::fromTextAndUrl($this->t('Campaign updates'), $campaignUpdateList);
    }

    // Campaign Transaction list.
    if (count($this->routeProvider->getRoutesByNames(['entity.campaign_transaction.collection'])) === 1) {
      $campaignTransactionList = Url::fromRoute('entity.campaign_transaction.collection');
      $links[] = Link::fromTextAndUrl($this->t('Campaign transactions'), $campaignTransactionList);
    }

    // Team list.
    if (count($this->routeProvider->getRoutesByNames(['entity.team.collection'])) === 1) {
      $teamList = Url::fromRoute('entity.team.collection');
      $links[] = Link::fromTextAndUrl($this->t('Teams'), $teamList);
    }

    // Non-Campaign Donation list.
    if (count($this->routeProvider->getRoutesByNames(['entity.donation.collection'])) === 1) {
      $donationList = Url::fromRoute('entity.donation.collection');
      $links[] = Link::fromTextAndUrl($this->t('Non-Campaign Donations'), $donationList);
    }

    $rows = [];
    foreach ($links as $link) {
      $rows[] = [
        '#markup' => $link,
      ];
    }

    $response['campaign_kit_table'] = [
      '#rows' => $rows,
      '#header' => [$this->t('Campaign Kit')],
      '#type' => 'table',
      '#empty' => $this->t('Please enable Campaign Kit Core module to see the menu.'),
    ];

    $links = [];

    // Campaign Types.
    if (count($this->routeProvider->getRoutesByNames(['entity.campaign_type.collection'])) === 1) {
      $campaignTypesForm = Url::fromRoute('entity.campaign_type.collection');
      $links[] = Link::fromTextAndUrl($this->t('Campaign types'), $campaignTypesForm);
    }

    // Team Types.
    if (count($this->routeProvider->getRoutesByNames(['entity.team_type.collection'])) === 1) {
      $teamTypesForm = Url::fromRoute('entity.team_type.collection');
      $links[] = Link::fromTextAndUrl($this->t('Team types'), $teamTypesForm);
    }

    // Campaign Kit Configuration Form.
    if (count($this->routeProvider->getRoutesByNames(['campaign_kit.configuration_form'])) === 1) {
      $campaignKitConfigForm = Url::fromRoute('campaign_kit.configuration_form');
      $links[] = Link::fromTextAndUrl($this->t('Campaign Kit configuration'), $campaignKitConfigForm);
    }

    $rows = [];
    foreach ($links as $link) {
      $rows[] = [
        '#markup' => $link,
      ];
    }

    $response['campaign_kit_config_table'] = [
      '#rows' => $rows,
      '#header' => [$this->t('Campaign Kit &mdash; Configuration')],
      '#type' => 'table',
      '#empty' => $this->t('No content available.'),
    ];

    // Add links only if demo module is installed.
    $rows = [];
    if ($this->moduleHandler->moduleExists('campaign_kit_demo')) {
      // Query demo Campaigns created by Campaign Kit Demo module.
      $ids = $this->entityTypeManager->getStorage('campaign')->getQuery('AND')
        ->condition('user_id', '0', '=')
        ->range('0', '3')
        ->execute();

      /* @var $entries \Drupal\campaign_kit_core\Entity\Campaign */
      $entries = Campaign::loadMultiple($ids);
      foreach ($entries as $entry => $value) {
        $campaignUrl = Url::fromRoute('campaign_kit_core.campaign_campaign_kit', ['campaign' => $value->id()]);
        $campaignLink = Link::fromTextAndUrl($value->label(), $campaignUrl);
        $rows[] = [
          '#markup' => $campaignLink,
        ];
      }

      // Get Badge Page link.
      // TODO: Revisit badges.
      $badgePage = Url::fromRoute('campaign_kit.campaign_kit_controller_badge_page');
      $linkBadgePage = Link::fromTextAndUrl($this->t('Badges'), $badgePage);

      $badges = [
        $linkBadgePage,
      ];

      foreach ($badges as $badge) {
        $rows[] = [
          '#markup' => $badge,
        ];
      }
    }

    $response['campaign_kit_demo_table'] = [
      '#rows' => $rows,
      '#header' => [$this->t('Campaign Kit &mdash;  Demonstration')],
      '#type' => 'table',
      '#empty' => $rows ? $this->t('No content available.') : $this->t('Please enable the Campaign Kit Demonstration module to see examples.'),
    ];

    return $response;
  }

  /**
   * Render a single Team entity.
   *
   * @param int $teamId
   *   The team id.
   *
   * @return array
   *   Return render array for specified team.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function teamView($teamId) {
    // Check if the Team entity exists.
    $teamId = $this->entityTypeManager->getStorage('team')->getQuery('AND')
      ->condition('id', $teamId, '=')
      ->execute();

    if ($teamId) {
      // Get the Team entity.
      /* @var $teamEntity \Drupal\campaign_kit_core\Entity\Team */
      $teamEntity = Team::load(array_shift($teamId));

      $teamArray = [];
      $teamArray['name'] = $teamEntity->getName();
      $teamArray['available'] = $teamEntity->getAvailable();
      $teamArray['is_published'] = $teamEntity->isPublished();

      $response = [
        '#theme' => 'team--display',
        '#team' => $teamArray,
      ];
    }
    else {
      $response = [
        '#markup' => 'This team does not exist.',
      ];

    }
    return $response;
  }

  /**
   * Render badge for this campaign.
   *
   * @return array
   *   Return render array for badge block.
   */
  public function badge() {
    // Check if demo module is enabled.
    $isEnabled = $this->moduleHandler->moduleExists('campaign_kit_demo');

    $renderedView = views_embed_view('campaign_kit_badges', [
      'display_id' => 'block_1',
      'check_access' => TRUE,
    ]);

    $renderedBadge = [
      '#theme' => 'campaign-badge',
      '#enable' => $isEnabled,
      '#badge' => $renderedView,
    ];

    return $renderedBadge;
  }

  /**
   * Create Child Campaign form.
   *
   * @return array
   *   Returns child form.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function addChild() {
    $campaign = $this->entityTypeManager->getStorage('campaign')
      ->create(['type' => 'child']);
    $childCreateForm = $this->entityFormBuilder->getForm($campaign, 'child');

    $childForm = [
      '#theme' => 'campaign--child--add',
      '#child_create_form' => $childCreateForm,
    ];
    return $childForm;
  }

  /**
   * Get Campaign title.
   *
   * @param \Drupal\Core\Entity\EntityInterface $campaign
   *   The Campaign entity.
   *
   * @return string
   *   Return the Campaign title.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getCampaignTitle(EntityInterface $campaign) {
    $campaignTitle = '';
    if ($campaign instanceof Campaign) {
      $campaignId = $campaign->id();
      // Check if the entity exists.
      $ids = $this->entityTypeManager->getStorage('campaign')->getQuery('AND')
        ->condition('id', $campaignId, '=')
        ->execute();

      if ($ids) {
        /* @var $campaignEntity \Drupal\campaign_kit_core\Entity\Campaign */
        $campaignEntity = Campaign::load($campaignId);
        $campaignTitle = $campaignEntity->get('title')->value;
      }
    }
    return $campaignTitle;
  }

  /**
   * Get Leaderboard table for a Campaign.
   *
   * @param \Drupal\Core\Entity\EntityInterface $campaign
   *   The Campaign entity.
   *
   * @return mixed
   *   Return a table.
   */
  public function getLeaderboardByCampaign(EntityInterface $campaign = NULL) {
    $rows = [];
    $headers = [];
    /* @var $campaign \Drupal\campaign_kit_core\Entity\Campaign */
    if ($campaign instanceof Campaign) {
      $campaign_id = $campaign->id();
      $leaderBoard = $this->t('Leaderboard');
      $response['#markup'] = $campaign->get('title')->value . ' ' . $leaderBoard;
      // TODO: Add service to Controller.
      $connection = $this->connection;
      $query = NULL;
      $query = $connection->select('team_field_data', 'tfd')->fields('tfd', ['id', 'name']);
      $query->addExpression("(SELECT max(created) FROM campaign_donation_field_data cdfd1 WHERE cdfd1.campaign_id = :campaign_id AND cdfd1.team_id=tfd.id)", 'lastDonationDate', [':campaign_id' => $campaign_id]);
      $query->addExpression("(SELECT count(name) FROM campaign_donation_field_data cdfd6 WHERE cdfd6.campaign_id = :campaign_id AND cdfd6.team_id=tfd.id)", 'totalDonors', [':campaign_id' => $campaign_id]);
      switch ($campaign->getFrequencyAllowed()) {
        case 'onetime':
          $headers = [
            $this->t('Team Name'),
            $this->t('# of members'),
            $this->t('Last Donation'),
            $this->t('One-Time Total'),
            $this->t('One-Time Donors'),
          ];
          $query->addExpression("(SELECT sum(amount) FROM campaign_donation_field_data cdfd2 WHERE cdfd2.campaign_id = :campaign_id AND cdfd2.team_id=tfd.id AND cdfd2.donation_frequency = 'onetime')", 'countOneTime', [':campaign_id' => $campaign_id]);
          $query->addExpression("(SELECT count(*) FROM campaign_donation_field_data cdfd3 WHERE cdfd3.campaign_id = :campaign_id AND cdfd3.team_id=tfd.id AND cdfd3.donation_frequency = 'onetime')", 'totalOneTime', [':campaign_id' => $campaign_id]);
          break;

        case 'monthly':
          $headers = [
            $this->t('Team Name'),
            $this->t('# of members'),
            $this->t('Last Donation'),
            $this->t('Monthly Total'),
            $this->t('Monthly Donors'),
          ];
          $query->addExpression("(SELECT sum(amount) FROM campaign_donation_field_data cdfd4 WHERE cdfd4.campaign_id = :campaign_id AND cdfd4.team_id=tfd.id AND cdfd4.donation_frequency = 'monthly')", 'countMonthly', [':campaign_id' => $campaign_id]);
          $query->addExpression("(SELECT count(*) FROM campaign_donation_field_data cdfd5 WHERE cdfd5.campaign_id = :campaign_id AND cdfd5.team_id=tfd.id AND cdfd5.donation_frequency = 'monthly')", 'totalMonthly', [':campaign_id' => $campaign_id]);
          break;

        case 'both':
          $headers = [
            $this->t('Team Name'),
            $this->t('# of members'),
            $this->t('Last Donation'),
            $this->t('One-Time Total'),
            $this->t('One-Time Donors'),
            $this->t('Monthly Total'),
            $this->t('Monthly Donors'),
          ];
          $query->addExpression("(SELECT sum(amount) FROM campaign_donation_field_data cdfd2 WHERE cdfd2.campaign_id = :campaign_id AND cdfd2.team_id=tfd.id AND cdfd2.donation_frequency = 'onetime')", 'countOneTime', [':campaign_id' => $campaign_id]);
          $query->addExpression("(SELECT count(*) FROM campaign_donation_field_data cdfd3 WHERE cdfd3.campaign_id = :campaign_id AND cdfd3.team_id=tfd.id AND cdfd3.donation_frequency = 'onetime')", 'totalOneTime', [':campaign_id' => $campaign_id]);
          $query->addExpression("(SELECT sum(amount) FROM campaign_donation_field_data cdfd4 WHERE cdfd4.campaign_id = :campaign_id AND cdfd4.team_id=tfd.id AND cdfd4.donation_frequency = 'monthly')", 'countMonthly', [':campaign_id' => $campaign_id]);
          $query->addExpression("(SELECT count(*) FROM campaign_donation_field_data cdfd5 WHERE cdfd5.campaign_id = :campaign_id AND cdfd5.team_id=tfd.id AND cdfd5.donation_frequency = 'monthly')", 'totalMonthly', [':campaign_id' => $campaign_id]);
          break;
      }

      $query->condition('campaign_id', $campaign_id, '=');
      // Order by name.
      $query->orderBy('name');

      $result = $query->execute();
      $records = $result->fetchAll();

      foreach ($records as $entry => $value) {
        /* @var $datetime \Drupal\Core\Datetime\DrupalDateTime */
        if ($value->lastDonationDate) {
          $lastDonationDate = $this->dateFormatter->format($value->lastDonationDate, 'short');
        }
        else {
          $lastDonationDate = '';
        }
        // Get currency from plugin.
        $config = $this->configFactory->get('campaign_kit.settings');
        $clientPlugin = $config->get('campaign_kit');
        $legendCurrency = $this->getCurrencySymbol($clientPlugin['currency']);
        $teamName = Link::createFromRoute(
          Team::load($value->id)->getName(),
          'entity.team.canonical',
          ['team' => $value->id]
        );

        switch ($campaign->getFrequencyAllowed()) {
          case 'onetime':
            $rows[] = [
              'id_team' => $teamName,
              'number_members' => $value->totalDonors,
              'last_donation_date' => $lastDonationDate,
              'count_one_time' => $value->countOneTime ? $legendCurrency . $value->countOneTime : '',
              'total_one_time' => $value->totalOneTime ? $value->totalOneTime : '',
            ];
            break;

          case 'monthly':
            $rows[] = [
              'id_team' => $teamName,
              'number_members' => $value->totalDonors,
              'last_donation_date' => $lastDonationDate,
              'count_monthly' => $value->countMonthly ? $legendCurrency . $value->countMonthly : '',
              'total_monthly' => $value->totalMonthly ? $value->totalMonthly : '',
            ];
            break;

          case 'both':
            $rows[] = [
              'id_team' => $teamName,
              'number_members' => $value->totalDonors,
              'last_donation_date' => $lastDonationDate,
              'count_one_time' => $value->countOneTime ? $legendCurrency . $value->countOneTime : '',
              'total_one_time' => $value->totalOneTime ? $value->totalOneTime : '',
              'count_monthly' => $value->countMonthly ? $legendCurrency . $value->countMonthly : '',
              'total_monthly' => $value->totalMonthly ? $value->totalMonthly : '',
            ];
            break;
        }
      }
    }

    $response['campaign_kit_demo_table'] = [
      '#rows' => $rows,
      '#header' => $headers,
      '#type' => 'table',
      '#empty' => $this->t('This Campaign does not have any team yet.'),
    ];

    $renderedLeaderboard = views_embed_view('leaderboard', [
      'display_id' => 'block_1',
      'check_access' => TRUE,
    ]);

    return $renderedLeaderboard;
  }

  /**
   * Gets the currency symbol given a currency code.
   *
   * @param string $currencyCode
   *   The Currency code to be validated.
   *
   * @return string
   *   The Currency Code.
   */
  private function getCurrencySymbol($currencyCode) {
    // TODO: Need central location for getCurrencySymbol().
    $currencySymbol = '';
    switch ($currencyCode) {
      case 'USD':
        $currencySymbol = '$';
        break;

    }
    return $currencySymbol;
  }

}
