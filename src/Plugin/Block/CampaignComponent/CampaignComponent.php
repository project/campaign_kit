<?php

namespace Drupal\campaign_kit\Plugin\Block\CampaignComponent;

use Drupal\campaign_kit_core\Entity\Campaign;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a 'CampaignComponent' block.
 *
 * @Block(
 *  id = "campaign_component",
 *  admin_label = @Translation("Campaign Component"),
 * )
 */
class CampaignComponent extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Path\AliasManagerInterface definition.
   *
   * @var \Drupal\Core\Path_alias\AliasManagerInterface
   */
  protected $pathAliasManager;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\Core\Extension\ModuleHandlerInterface definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Drupal\Core\Path\CurrentPathStack definition.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $pathCurrent;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\Core\Routing\CurrentRouteMatch service.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * The Messenger Service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new CampaignComponent object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager.
   * @param \Drupal\Core\Path\AliasManagerInterface $path_alias_manager
   *   The Path Alias Manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Config Factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The Current User.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The Module Handler.
   * @param \Drupal\Core\Path\CurrentPathStack $path_current
   *   The Current Path.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The Request.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $currentRouteMatch
   *   The Current Route Match service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Messenger Service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    AliasManagerInterface $path_alias_manager,
    ConfigFactoryInterface $config_factory,
    AccountProxyInterface $current_user,
    ModuleHandlerInterface $module_handler,
    CurrentPathStack $path_current,
    Request $request,
    CurrentRouteMatch $currentRouteMatch,
    MessengerInterface $messenger
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->pathAliasManager = $path_alias_manager;
    $this->configFactory = $config_factory;
    $this->currentUser = $current_user;
    $this->moduleHandler = $module_handler;
    $this->pathCurrent = $path_current;
    $this->requestStack = $request;
    $this->currentRouteMatch = $currentRouteMatch;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('path_alias.manager'),
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('module_handler'),
      $container->get('path.current'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('current_route_match'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    // Add template.
    $build['#theme'] = 'CampaignComponent';

    // Get Campaign Entity from route.
    $currentRoute = $this->currentRouteMatch;
    $campaign = $currentRoute->getParameters()->get('campaign');

    // Local variables.
    $campaignArray = [];
    if ($campaign instanceof Campaign) {
      $campaignId = $campaign->id();

      // Check if the entity exists.
      $ids = $this->entityTypeManager->getStorage('campaign')->getQuery('AND')
        ->condition('id', $campaignId, '=')
        ->execute();
    }

    if ($campaign instanceof Campaign) {
      if ($ids) {
        // View builder.
        $entityType = 'campaign';
        $viewDisplay = '';

        // Fields to render.
        $fields = [
          'title',
          'parent_id',
          'campaign_type',
          'header_image',
          'badge_image',
          'confirmation_email',
          'description',
          'short_description',
        ];

        // Get the Campaign entity object.
        /* @var $campaignEntity \Drupal\campaign_kit_core\Entity\Campaign */
        $campaignEntity = Campaign::load($campaignId);
        // Get the View for this Campaign entity object.
        $viewBuilder = $this->entityTypeManager->getViewBuilder($entityType);

        // Build the Campaign array to render.
        foreach ($fields as $field_name) {
          if ($campaignEntity->hasField($field_name) && $campaignEntity->access('view')) {
            $value = $campaignEntity->get($field_name);
            $output = $viewBuilder->viewField($value, $viewDisplay);
            $output['#cache']['tags'] = $campaignEntity->getCacheTags();
            $campaignArray[$field_name] = $output;
          }
        }
        // Get the campaign entity.
        // Build the Campaign object.
        $campaignArray['id'] = $campaignEntity->getCampaignId();
        $campaignArray['one_time_donation_goal'] = (int) $campaignEntity->getGoal();
        $campaignArray['monthly_donation_goal'] = (int) $campaignEntity->getGoalSustaining();
        $campaignArray['campaign_status'] = $campaignEntity->getCampaignStatus();
        $campaignArray['facebook_url'] = $campaignEntity->getFacebookPath();
        $campaignArray['twitter_url'] = $campaignEntity->getTwitterPath();
        $campaignArray['instagram_url'] = $campaignEntity->getInstagramPath();
        $campaignArray['donation_frequency_allowed'] = $campaignEntity->getFrequencyAllowed();
        $campaignArray['campaign_type'] = $campaignEntity->getCampaignType();
        $campaignArray['days_left'] = $campaignEntity->getDaysLeft();
        $campaignArray['goal'] = $campaignEntity->getGoal();
        $campaignArray['author_name'] = $campaignEntity->getOwnerName();
        $campaignArray['use_teams'] = $campaignEntity->get('use_teams')->value;
        $campaignArray['is_open'] = $campaignEntity->isCampaignOpen();
        // Get alias.
        $aliasCampaign = $this->pathAliasManager->getAliasByPath('/campaign/' . $campaignId);
        if ($aliasCampaign !== '') {
          $campaignArray['alias'] = $aliasCampaign;
        }

        // Get Campaign's money information.
        $campaignArray['num_supporters'] = $campaignEntity->getNumberSupporters();
        $campaignArray['goal_saved'] = $campaignEntity->getRaisedAmount();
        $campaignArray['percentage'] = $campaignEntity->getPercentage();
        // Get currency from plugin.
        $config = $this->configFactory->get('campaign_kit.settings');
        $clientPlugin = $config->get('campaign_kit');
        $campaignArray['currency'] = $clientPlugin['currency'];

        // Create links for Campaign Page.
        // I want to participate link destination.
        $destination = Url::fromRoute('campaign_kit.campaign_kit_controller_add_child_page', ['campaignParentId' => $campaignArray['id']]);

        $loginUrl = Url::fromRoute(
          'user.login',
          [],
          [
            'query' => ['destination' => $destination->getInternalPath()],
          ]
        );

        if ($this->currentUser->isAnonymous()) {
          $linkParticipate = Link::fromTextAndUrl($this->t('I Want to Join this Campaign!'), $loginUrl);
        }
        else {
          $linkParticipate = Link::fromTextAndUrl($this->t('I Want to Join this Campaign!'), $destination);
        }

        $campaignArray['participate_link'] = $linkParticipate->toRenderable();
        $campaignArray['participate_link']['#attributes'] = ['class' => ['donate-button']];

        // Donate link.
        // campaign_kit_core.campaign_donate.
        // Check if Campaign is OPEN.
        if ($campaignEntity->isCampaignOpen()) {
          $donationUrl = Url::fromRoute('campaign_kit_core.campaign_donate', ['campaign' => $campaignArray['id']]);
          $donationLink = Link::fromTextAndUrl($this->t('Donate Now'), $donationUrl);
          $campaignArray['donate_link'] = $donationLink->toRenderable();
          $campaignArray['donate_link']['#attributes'] = ['class' => ['donate-button']];
        }
        else {
          $closedMessage = $this->t('This campaign is closed');
          $campaignArray['donate_link']['#markup'] = '<label>'. $closedMessage . '</label>';
          $campaignArray['donate_link']['#attributes'] = ['class' => ['donate-button']];
        }

        // Create social network links.
        if ($campaignEntity->getFacebookPath()) {
          $facebookUrl = Url::fromUri($campaignEntity->getFacebookPath());
          $facebookLink = Link::fromTextAndUrl($this->t('Share on Facebook'), $facebookUrl);
          $campaignArray['facebook_link'] = $facebookLink->toRenderable();
          $campaignArray['facebook_link']['#attributes'] = [
            'class' => ['facebook-icon'],
            'target' => '_blank',
            'title' => $this->t('Share on Facebook'),
          ];
        }

        if ($campaignEntity->getTwitterPath()) {
          $twitterUrl = Url::fromUri($campaignEntity->getTwitterPath());
          $twitterLink = Link::fromTextAndUrl($this->t('Share on Twitter'), $twitterUrl);
          $campaignArray['twitter_link'] = $twitterLink->toRenderable();
          $campaignArray['twitter_link']['#attributes'] = [
            'class' => ['twitter-icon'],
            'target' => '_blank',
            'title' => $this->t('Share on Twitter'),
          ];
        }

        if ($campaignEntity->getInstagramPath()) {
          $instagramUrl = Url::fromUri($campaignEntity->getInstagramPath());
          $instagramLink = Link::fromTextAndUrl($this->t('Share on Instagram'), $instagramUrl);
          $campaignArray['instagram_link'] = $instagramLink->toRenderable();
          $campaignArray['instagram_link']['#attributes'] = [
            'class' => ['instagram-icon'],
            'target' => '_blank',
            'title' => $this->t('Share on Instagram'),
          ];
        }

        // Get the alias for the current campaign.
        $isItEnabled = $this->moduleHandler->moduleExists('pathauto');
        $routeName = $this->pathCurrent->getPath();
        if ($isItEnabled) {
          $result = $this->pathAliasManager->getAliasByPath($routeName);
          if ($result) {
            $bodyLink = $result;
          }
          else {
            $bodyLink = $routeName;
          }
        }
        else {
          $bodyLink = $routeName;
        }
        $hostHttp = $this->requestStack->getSchemeAndHttpHost();
        $bodyLink = $hostHttp . $bodyLink;
        // Current URL.
        $campaignArray['current_url'] = $bodyLink;

        // Create link for email a campaign.
        $subject = $this->t('Check out this fantastic campaign!');
        $body = $this->t('Have a look at this campaign and please help if you are inspired to!');
        $friendUrl = Url::fromUri('mailto:?subject=' . $subject . '&body=' . $body . '%0D%0A' . $bodyLink);
        $friendLink = Link::fromTextAndUrl($this->t('Email a friend'), $friendUrl);
        $campaignArray['email_friend_link'] = $friendLink->toRenderable();
        $campaignArray['email_friend_link']['#attributes'] = [
          'class' => ['email-icon'],
          'title' => $this->t('Email a friend'),
        ];

        // Create Subscribe link.
        // TODO: Add subscribe link to campaigns.
        // $subscribeUrl = Url::fromUri('https://www.facebook.com/');
        // $subscribeLink = Link::fromTextAndUrl($this->t('Subscribe to Updates'), $subscribeUrl);
        // $campaignArray['subscribe_link'] = $subscribeLink->toRenderable();
        // $campaignArray['subscribe_link']['#attributes'] = [
        //  'class' => ['subscribe-button'],
        //  'title' => $this->t('Subscribe to Updates'),
        // ];
        // Create Continue to homepage link.
        $homeUrl = Url::fromRoute('<front>');
        $homeLink = Link::fromTextAndUrl($this->t('Continue to homepage'), $homeUrl);
        $campaignArray['home_page_link'] = $homeLink->toRenderable();
        $campaignArray['home_page_link']['#attributes'] = [
          'class' => ['default-button'],
          'title' => $this->t('Continue to homepage'),
        ];

        // Get views and add them to Campaign array.
        $renderSupporters = views_embed_view('campaign_kit_supporters', [
          'display_id' => 'block_1',
          'check_access' => TRUE,
        ]);
        $renderCampaignUpdates = views_embed_view('campaign_kit_updates', [
          'display_id' => 'block_1',
          'check_access' => TRUE,
        ]);

        $campaignArray['supporters_view'] = $renderSupporters;
        $campaignArray['updates_view'] = $renderCampaignUpdates;

        // Render Badge Component.
        $campaignArray['badge'] = $this->getBadges($campaignId);
      }
      else {
        $this->messenger->addError($this->t("Unable to find campaign."));
      }
    }
    else {
      $build['#campaign_message'] = $this->t('This will show the Campaign page once you go to the view page.');
      $build['#campaign_message_warning'] = $this->t('This component will be shown only in Campaign entity pages.');
    }
    $build['#campaign'] = $campaignArray;

    return $build;
  }

  /**
   * @param $campaignId
   * @return array
   */
  protected function getBadges($campaignId) {
    // Render Badge only for 'onetime'
    $blockManager = \Drupal::service('plugin.manager.block');
    $config = [
      'campaign' => $campaignId
    ];
    $pluginBlock = $blockManager->createInstance('badge_component', $config);

    $accessResult = $pluginBlock->access(\Drupal::currentUser());
    // If we want to check permission per user.
    if (is_object($accessResult) && $accessResult->isForbidden() || is_bool($accessResult) && !$accessResult) {
      return [];
    }

    return $pluginBlock->build();
  }

}
