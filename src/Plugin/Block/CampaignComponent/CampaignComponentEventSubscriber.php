<?php

namespace Drupal\campaign_kit\Plugin\Block\CampaignComponent;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\core_event_dispatcher\Event\Theme\ThemeEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class CampaignComponentEventSubscriber.
 *
 * @package Drupal\campaign_kit
 */
class CampaignComponentEventSubscriber implements EventSubscriberInterface {

  /**
   * Logger Factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * DonateComponentEventSubscriber constructor.
   */
  public function __construct(LoggerChannelFactoryInterface $loggerFactory) {
    $this->loggerFactory = $loggerFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];

    $events[HookEventDispatcherInterface::THEME][] = ['themeEvent'];
    return $events;
  }

  /**
   * Theme event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\Theme\ThemeEvent $event
   *   The event.
   */
  public function themeEvent(ThemeEvent $event) {
    $modulePath = drupal_get_path('module', 'campaign_kit');

    $newThemes = [
      'CampaignComponent' => [
        'template' => 'CampaignComponent',
        'render element' => 'children',
        'variables' => [
          'campaign' => [],
          'campaign_message' => NULL,
          'campaign_message_warning' => NULL,
        ],
        'path' => $modulePath . '/src/Plugin/Block/CampaignComponent',
      ],
    ];

    $event->addNewThemes($newThemes);
  }

}
