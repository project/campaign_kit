<?php

namespace Drupal\campaign_kit\Plugin\Block\SimpleThankYou;

use Drupal\campaign_kit_core\Entity\Campaign;
use Drupal\campaign_kit_core\Entity\CampaignDonation;
use Drupal\campaign_kit_core\Entity\Donation;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\layout_builder\Field\LayoutSectionItemList;
use Drupal\layout_builder\Section;
use Drupal\layout_builder\SectionComponent;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'SimpleThankYou' block.
 *
 * @Block(
 *  id = "simple_thank_you",
 *  admin_label = @Translation("Simple Thank you"),
 * )
 */
class SimpleThankYou extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Routing\CurrentRouteMatch service.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * The Messenger Service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new Donation object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The Entity Type Manager service.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $currentRouteMatch
   *   The Current Route Match service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Messenger Service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, CurrentRouteMatch $currentRouteMatch, MessengerInterface $messenger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->currentRouteMatch = $currentRouteMatch;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $configurationBlock = $configuration;
    $pluginId = $plugin_id;
    $pluginDefinition = $plugin_definition;
    $entityTypeManager = $container->get('entity_type.manager');
    $currentRouteMatch = $container->get('current_route_match');
    $messenger = $container->get('messenger');

    return new static(
      $configurationBlock,
      $pluginId,
      $pluginDefinition,
      $entityTypeManager,
      $currentRouteMatch,
      $messenger
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['use_thank_you'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use thank you'),
      '#default_value' => $this->configuration['use_thank_you'],
      '#weight' => '5',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['use_thank_you'] = $form_state->getValue('use_thank_you');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    // Add template.
    $build['#theme'] = 'SimpleThankYou';
    // Attach library.
    $build['#attached']['library'] = ['campaign_kit/thank-you-component-styling'];

    $build['#thank_you_use_thank_you']['#markup'] = '<p>' . $this->configuration['use_thank_you'] . '</p>';

    // Render thank you block for simple thank you component.
    // Local variables.
    $thankYou = [];
    $donateComponent = [];

    // Get Donation ID from url '?donation=id'.
    $donationId = \Drupal::request()->query->get('donation');

    // Get Node ID from url '?node=id'.
    $nodeId = \Drupal::request()->query->get('node');

    // Get Node's configuration.
    if (is_numeric($nodeId)) {
      // Test if Node has all inside.
      $node = Node::load($nodeId);
      $layoutBuilderNode = $node->get('layout_builder__layout');
      if ($layoutBuilderNode instanceof LayoutSectionItemList) {
        $moreProperties = $layoutBuilderNode->getValue();
        $section = $moreProperties[0]['section'];
        if ($section instanceof Section) {
          $components = $section->getComponents();
          foreach ($components as $component) {
            if ($component instanceof SectionComponent) {
              $configForEach = $component->get('configuration');
              if ($configForEach['id'] == 'donate_component') {
                // This has all configuration for Donate Component.
                $donateComponent = $configForEach;
              }
            }
          }
        }
      }
    }

    // Build Simple Thank You component.
    if (is_numeric($donationId)) {
      // Get the Donation entity object.
      /* @var $donationEntity \Drupal\campaign_kit_core\Entity\Campaign */
      $donationEntity = Donation::load($donationId);
      if ($donationEntity instanceof Donation) {
        // Getting just values.
        $thankYou['dedicate_type'] = $donationEntity->get('dedicate_type')->value;
        $thankYou['honoree_first_name'] = $donationEntity->get('honoree_first_name')->value;
        $thankYou['honoree_last_name'] = $donationEntity->get('honoree_last_name')->value;
        $thankYou['recipient_mail'] = $donationEntity->get('recipient_mail')->value;
        $thankYou['recipient_first_name'] = $donationEntity->get('recipient_first_name')->value;
        $thankYou['recipient_last_name'] = $donationEntity->get('recipient_last_name')->value;
        $thankYou['dedicate_donation'] = $donationEntity->get('dedicate_donation')->value;
      }
    }

    // TODO: Ask Andre if this can go in Simple Thank You form.
    $thankYou['thank_you_message'] = $donateComponent['thank_you_message']['value'];
    $thankYou['thank_you_message_format'] = $donateComponent['thank_you_message']['format'];
    // Build the image.
    if (!empty($donateComponent['background_image'])) {
      $imageFileId = implode($donateComponent['background_image']);
      $image = File::load($imageFileId);
      if ($image != NULL) {
        $image->setPermanent();
        $image->save();

        if ($donateComponent['image_style'] == "none") {
          $thankYou['header_image'] = [
            '#theme' => 'image',
            '#uri' => $image->getFileUri(),
          ];
        }
        else {
          $thankYou['header_image'] = [
            '#theme' => 'image_style',
            //Medium
            '#style_name' => $donateComponent['image_style'],
            //'#style_name' => 'medium',
            '#uri' => $image->getFileUri(),
          ];
        }
      }
    }

    $build['#thank_you_component'] = $thankYou;

    return $build;
  }

}
