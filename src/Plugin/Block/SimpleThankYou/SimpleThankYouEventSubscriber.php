<?php

namespace Drupal\campaign_kit\Plugin\Block\SimpleThankYou;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\core_event_dispatcher\Event\Theme\ThemeEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class SimpleThankYouEventSubscriber.
 *
 * @package Drupal\campaign_kit
 */
class SimpleThankYouEventSubscriber implements EventSubscriberInterface {

  /**
   * Logger Factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * SimpleThankYouEventSubscriber constructor.
   */
  public function __construct(LoggerChannelFactoryInterface $loggerFactory) {
    $this->loggerFactory = $loggerFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];

    $events[HookEventDispatcherInterface::THEME][] = ['themeEvent'];
    return $events;
  }

  /**
   * Theme event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\Theme\ThemeEvent $event
   *   The event.
   */
  public function themeEvent(ThemeEvent $event) {
    $modulePath = drupal_get_path('module', 'campaign_kit');

    $newThemes = [
      'SimpleThankYou' => [
        'template' => 'SimpleThankYou',
        'render element' => 'content',
        'variables' => [
          'thank_you_use_thank_you' => NULL,
          'thank_you_component' => [],
        ],
        'path' => $modulePath . '/src/Plugin/Block/SimpleThankYou',
      ],
    ];

    $event->addNewThemes($newThemes);
  }

}
