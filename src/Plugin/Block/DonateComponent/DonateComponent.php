<?php

namespace Drupal\campaign_kit\Plugin\Block\DonateComponent;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Entity\Plugin\DataType\ConfigEntityAdapter;
use Drupal\Core\Entity\Plugin\DataType\EntityAdapter;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\Context\EntityContext;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\layout_builder\Entity\LayoutBuilderEntityViewDisplay;
use Drupal\layout_builder\Plugin\SectionStorage\DefaultsSectionStorage;
use Drupal\layout_builder\Plugin\SectionStorage\OverridesSectionStorage;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'DonateComponent' block.
 *
 * @Block(
 *  id = "donate_component",
 *  admin_label = @Translation("Donate Component"),
 * )
 */
class DonateComponent extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Form\FormBuilderInterface definition.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Drupal\Core\Config\ConfigManagerInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * Drupal\Core\Routing\CurrentRouteMatch class.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * Constructs a new Donate Component object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The Form Builder service.
   * @param \Drupal\Core\Config\ConfigManagerInterface $configManager
   *   The ConfigManagerInterface service.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $currentRouteMatch
   *   The CurrentRouteMatch service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $form_builder, ConfigManagerInterface $configManager, CurrentRouteMatch $currentRouteMatch) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
    $this->configManager = $configManager;
    $this->currentRouteMatch = $currentRouteMatch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $configurationBlock = $configuration;
    $pluginId = $plugin_id;
    $pluginDefinition = $plugin_definition;
    $formBuilder = $container->get('form_builder');
    $configManager = $container->get('config.manager');
    $currentRouteMatch = $container->get('current_route_match');

    return new static(
      $configurationBlock,
      $pluginId,
      $pluginDefinition,
      $formBuilder,
      $configManager,
      $currentRouteMatch
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    // Get value from CampaignKitSettingsForm's configuration.
    $configFactory = $this->configManager->getConfigFactory();
    $locationFolderConfig = $configFactory->get('campaign_kit.settings');
    $locationFolder = $locationFolderConfig->get('campaign_kit.image_location');

    $form['include_teams'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include teams?'),
      '#default_value' => $this->configuration['include_teams']?: TRUE,
      '#weight' => '5',
      '#required' => TRUE,
    ];

    // Thank You background image.
    $form['background_image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Background Image'),
      '#upload_location' => ($locationFolder != NULL) ? 'public://' . $locationFolder : 'public://donate-component',
      '#multiple' => FALSE,
      '#default_value' => $this->configuration['background_image'],
      '#required' => TRUE,
      '#upload_validators' => [
        'file_validate_extensions' => [$locationFolderConfig->get('campaign_kit.image_extensions')],
      ],
      '#weight' => '3',
    ];

    $form['image_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Image style'),
      '#options' => [
        'none' => $this->t('None'),
        'thumbnail' => $this->t('Thumbnail'),
        'medium' => $this->t('Medium'),
        'large' => $this->t('Large'),
      ],
      '#default_value' => $this->configuration['image_style'],
      '#weight' => '4',
    ];

    // Thank You message field.
    $textValue = '';
    if (isset($this->configuration['thank_you_message']['value'])) $textValue = $this->configuration['thank_you_message']['value'];

    $textFormat = '';
    if (isset($this->configuration['thank_you_message']['format'])) $textFormat = $this->configuration['thank_you_message']['format'];
    $form['thank_you_message'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Thank You message'),
      '#default_value' => $textValue,
      '#format' => $textFormat,
      '#required' => TRUE,
      '#weight' => '5',
    ];

    $form['thank_you_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Thank you URL"),
      '#default_value' => $this->configuration['thank_you_url'],
      '#maxlength' => '60',
      '#description' => $this->t('Example: /home, https://www.drupal.org/'),
      '#attributes' => ['placeholder' => $this->t('Thank you URL')],
      '#weight' => '6',
      '#required' => TRUE,
    ];

    $form['donate_failed_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Donate Failed URL"),
      '#default_value' => $this->configuration['donate_failed_url'],
      '#maxlength' => '60',
      '#description' => $this->t('Example: /home, https://www.drupal.org/'),
      '#attributes' => ['placeholder' => $this->t('Donate Failed URL')],
      '#weight' => '7',
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['include_teams'] = $form_state->getValue('include_teams');
    $this->configuration['background_image'] = $form_state->getValue('background_image');
    $this->configuration['image_style'] = $form_state->getValue('image_style');
    $this->configuration['thank_you_message'] = $form_state->getValue('thank_you_message');
    $this->configuration['thank_you_url'] = $form_state->getValue('thank_you_url');
    $this->configuration['donate_failed_url'] = $form_state->getValue('donate_failed_url');

    // Get the node ID according the context and Set the Node ID.
    $content = $this->getContextNode($this->currentRouteMatch);
    if (isset($content['node'])) {
      if ($content['node'] instanceof Node) {
        $this->configuration['node_id'] = $content['node']->id();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    // Add template.
    $build['#theme'] = 'DonateComponent';
    // Attach library.
    $build['#attached']['library'] = ['campaign_kit/donate-component-styling'];

    $build['#include_teams']['#markup'] = '<p>' . $this->configuration['include_teams'] . '</p>';

    // Get Donate form and pass parameters.
    $form = $this->formBuilder->getForm('Drupal\campaign_kit\Form\DonateForm', $this->configuration['thank_you_url'], $this->configuration['donate_failed_url'], $this->configuration['node_id']);
    if ($form) {
      $build['#donate_form'] = $form;
    }

    return $build;
  }

  /**
   * Get node from section (won't work in Layout Builder if not from section).
   *
   * @param $currentRouteMatch
   * @return array
   *   Return an array.
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getContextNode(CurrentRouteMatch $currentRouteMatch) {
    $contentContext = [];

    if ($currentRouteMatch->getParameter('node')) {
      $contentContext['node'] = $currentRouteMatch->getParameter('node');
      $contentContext['source'] = 'route';
      $contentContext['bundle'] = $contentContext['node']->getType();
    }
    else {
      // Individual layout.
      $sectionStorage = $currentRouteMatch->getParameters()->get('section_storage');
      if($sectionStorage instanceof OverridesSectionStorage) {
        $context = $sectionStorage->getContext('entity');
        if($context instanceof EntityContext) {
          $contextData = $context->getContextData();
          if($contextData instanceof EntityAdapter) {
            $contentContext['node'] = $contextData->getEntity();
            $contentContext['source'] = 'individual_layout';
            $contentContext['bundle'] = $contentContext['node']->getType();
          }
        }
      }
      else {
        // Content type layout.
        if ($sectionStorage instanceof DefaultsSectionStorage) {
          $availableContexts = $sectionStorage->getContexts();
          $contextDisplay = $availableContexts['display'];
          if ($contextDisplay instanceof EntityContext) {
            $contextData = $contextDisplay->getContextData();
            if ($contextData instanceof ConfigEntityAdapter) {
              $contentInfo = $contextData->getEntity();
              if ($contentInfo instanceof LayoutBuilderEntityViewDisplay) {
                $contentContext['source'] = 'entity_type_layout';
                $contentContext['bundle'] = $contentInfo->getTargetBundle();
              }
            }
          }
        }
      }
    }
    return $contentContext;
  }

}
