<?php

namespace Drupal\campaign_kit\Plugin\Block\ThankYou;

use Drupal\campaign_kit_core\Entity\Campaign;
use Drupal\campaign_kit_core\Entity\CampaignDonation;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'ThankYou' block.
 *
 * @Block(
 *  id = "thank_you",
 *  admin_label = @Translation("Thank you"),
 * )
 */
class ThankYou extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Routing\CurrentRouteMatch service.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * The Messenger Service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new Donation object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The Entity Type Manager service.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $currentRouteMatch
   *   The Current Route Match service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Messenger Service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, CurrentRouteMatch $currentRouteMatch, MessengerInterface $messenger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->currentRouteMatch = $currentRouteMatch;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $configurationBlock = $configuration;
    $pluginId = $plugin_id;
    $pluginDefinition = $plugin_definition;
    $entityTypeManager = $container->get('entity_type.manager');
    $currentRouteMatch = $container->get('current_route_match');
    $messenger = $container->get('messenger');

    return new static(
      $configurationBlock,
      $pluginId,
      $pluginDefinition,
      $entityTypeManager,
      $currentRouteMatch,
      $messenger
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['use_thank_you'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use thank you'),
      '#default_value' => $this->configuration['use_thank_you'],
      '#weight' => '5',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['use_thank_you'] = $form_state->getValue('use_thank_you');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    // Add template.
    $build['#theme'] = 'thankYou';
    // Attach library.
    $build['#attached']['library'] = ['campaign_kit/thank-you-component-styling'];

    $build['#thank_you_use_thank_you']['#markup'] = '<p>' . $this->configuration['use_thank_you'] . '</p>';

    // Render thank you block for campaign.
    // Local variables.
    $campaignArray = [];
    $campaignId = NULL;

    // Get Campaign Entity from route.
    $currentRoute = $this->currentRouteMatch;
    $campaign = $currentRoute->getParameters()->get('campaign');
    $campaignDonationId = $currentRoute->getParameters()->get('campaignDonationId');
    if ($campaign instanceof Campaign) {
      $campaignId = $campaign->id();
    }

    // Get Campaign Entity.
    if (is_numeric($campaignId)) {
      // Check if the entity exists.
      $ids = $this->entityTypeManager->getStorage('campaign')->getQuery('AND')
        ->condition('id', $campaignId, '=')
        ->execute();

      if ($ids) {
        // View builder for Campaign Entity.
        $entityType = 'campaign';
        $viewDisplay = '';

        // Fields to render Campaign Entity.
        $fields = [
          'title',
          'parent_id',
          'campaign_type',
          'header_image',
          'mail',
          'thank_you_message',
          'donation_page_title',
          'donation_page_description',
        ];

        // Get the Campaign entity object.
        /* @var $campaignEntity \Drupal\campaign_kit_core\Entity\Campaign */
        $campaignEntity = Campaign::load($campaignId);
        // Get the ViewBuilder for Campaign entity.
        $viewBuilder = $this->entityTypeManager->getViewBuilder($entityType);

        // Build the array to render fields.
        foreach ($fields as $field_name) {
          if ($campaignEntity->hasField($field_name) && $campaignEntity->access('view')) {
            $value = $campaignEntity->get($field_name);
            $output = $viewBuilder->viewField($value, $viewDisplay);
            $output['#cache']['tags'] = $campaignEntity->getCacheTags();
            $campaignArray[$field_name] = $output;
          }
        }
        // Check if it is a parent or child.
        if ($campaignEntity->get('campaign_type')->value == 'child') {
          // Back to campaign link.
          $backToCampaignUrl = Url::fromRoute('campaign_kit_core.campaign_campaign_kit', [
            'campaign' => $campaignId,
          ]);
          $backToCampaignLink = Link::fromTextAndUrl($this->t('Back to Campaign Page!'), $backToCampaignUrl);
          $campaignArray['back_to_campaign_link'] = $backToCampaignLink->toRenderable();
          $campaignArray['back_to_campaign_link']['#attributes'] = [
            'class' => [
              'donate-button',
              'campaign-default--button',
            ],
          ];

        }
        else {
          // Back to campaign link.
          $backToCampaignUrl = Url::fromRoute('campaign_kit_core.campaign_campaign_kit', [
            'campaign' => $campaignId,
          ]);
          $backToCampaignLink = Link::fromTextAndUrl($this->t('Back to Campaign Page!'), $backToCampaignUrl);
          $campaignArray['back_to_campaign_link'] = $backToCampaignLink->toRenderable();
          $campaignArray['back_to_campaign_link']['#attributes'] = [
            'class' => [
              'donate-button',
              'campaign-default--button',
            ],
          ];
        }

        // Query Campaign Donation fields.
        $campaignDonationId = $this->entityTypeManager->getStorage('campaign_donation')->getQuery('AND')
          ->condition('id', $campaignDonationId, '=')
          ->condition('campaign_id', $campaignId, '=')
          ->execute();

        if ($campaignDonationId) {
          // Load Campaign Donation Entity.
          /* @var $campaignDonationEntity \Drupal\campaign_kit_core\Entity\CampaignDonation */
          $campaignDonationEntity = CampaignDonation::load(reset($campaignDonationId));

          // View builder for Campaign Donation Entity.
          $campaignDonationEntityType = 'campaign_donation';
          $viewModeCampaignDonation = 'default';

          // Fields to render Campaign Donation Entity.
          $fieldsCampaignDonation = [
            'payment_processor',
            'recipient_message',
          ];

          // Get the ViewBuilder for Campaign Donation Entity.
          $viewBuilderCampaignDonation = $this->entityTypeManager->getViewBuilder($campaignDonationEntityType);

          // Build the array to render.
          foreach ($fieldsCampaignDonation as $field_name) {
            if ($campaignDonationEntity->hasField($field_name) && $campaignDonationEntity->access('view')) {
              $value2 = $campaignDonationEntity->get($field_name);
              $output2 = $viewBuilderCampaignDonation->viewField($value2, $viewModeCampaignDonation);
              $output2['#cache']['tags'] = $campaignDonationEntity->getCacheTags();
              $campaignArray[$field_name] = $output2;
            }
          }

          // Set True or False for 'dedicate_donation' field.
          if ($campaignDonationEntity->get('dedicate_donation')->value == '1') {
            $campaignArray['dedicate_donation'] = TRUE;
          }
          else {
            $campaignArray['dedicate_donation'] = FALSE;
          }
          // Getting just values.
          $campaignArray['dedicate_type'] = $campaignDonationEntity->get('dedicate_type')->value;
          $campaignArray['honoree_first_name'] = $campaignDonationEntity->get('honoree_first_name')->value;
          $campaignArray['honoree_last_name'] = $campaignDonationEntity->get('honoree_last_name')->value;
          $campaignArray['recipient_mail'] = $campaignDonationEntity->get('recipient_mail')->value;
          $campaignArray['recipient_first_name'] = $campaignDonationEntity->get('recipient_first_name')->value;
          $campaignArray['recipient_last_name'] = $campaignDonationEntity->get('recipient_last_name')->value;

        }
        else {
          $this->messenger->addError($this->t("There are no donations for this campaign yet."));
        }

      }
      else {
        $this->messenger->addError($this->t("Unable to find campaign. Please check the correct Campaign."));
      }
    }
    else {
      $this->messenger->addError($this->t("Unable to find campaign. Please check the correct Campaign."));
    }

    $build['#thank_you_component'] = $campaignArray;

    return $build;
  }

}
