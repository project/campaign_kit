<?php

namespace Drupal\campaign_kit\Plugin\Block\BadgeComponent;

use Drupal\campaign_kit_core\Entity\Campaign;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigManagerInterface;

/**
 * Provides a 'BadgeComponent' block.
 *
 * @Block(
 *  id = "badge_component",
 *  admin_label = @Translation("Badge Component"),
 * )
 */
class BadgeComponent extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Config\ConfigManagerInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * Drupal\Core\Routing\CurrentRouteMatch service.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * The Messenger Service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new BadgeComponent object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The EntityTypeManagerInterface definition.
   * @param \Drupal\Core\Config\ConfigManagerInterface $config_manager
   *   The ConfigManagerInterface definition.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $currentRouteMatch
   *   The Current Route Match service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Messenger Service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Config Factory.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigManagerInterface $config_manager,
    CurrentRouteMatch $currentRouteMatch,
    MessengerInterface $messenger,
    ConfigFactoryInterface $config_factory
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->configManager = $config_manager;
    $this->currentRouteMatch = $currentRouteMatch;
    $this->messenger = $messenger;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('config.manager'),
      $container->get('current_route_match'),
      $container->get('messenger'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['campaign'] = [
      '#type' => 'select',
      '#title' => $this->t('Campaign'),
      '#options' => $this->getCampaigns(),
      '#default_value' => $this->configuration['campaign'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['campaign'] = $form_state->getValue('campaign');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    // Add template.
    $build['#theme'] = 'BadgeComponent';
    // Attach library.
    $build['#attached']['library'] = ['campaign_kit/badge-component-styling'];

    /*
    $build['#text']['#markup'] = '<p>Hello I am Badge Component markup</p>';
    $build['#campaign']['#markup'] = '<p>' . $this->configuration['campaign'] . 'ss</p>';
    */

    // TODO: Get badge.
    // Get Campaign Entity from route.
    $currentRoute = $this->currentRouteMatch;
    //$campaign = $currentRoute->getParameters()->get('campaign');
    if (!isset($campaign)) {
      $campaign = Campaign::load($this->configuration['campaign']);
    }

    $campaignBadge = [];
    if (isset($campaign)) {
      if ($campaign instanceof Campaign) {
        // View builder.
        $entityType = 'campaign';
        $viewDisplay = '';

        // Fields to render.
        $fields = [
          'title',
          'parent_id',
          'campaign_type',
          'header_image',
          'badge_image',
          'confirmation_email',
          'description',
          'short_description',
        ];

        // Get the Campaign entity object.
        /* @var $campaignEntity \Drupal\campaign_kit_core\Entity\Campaign */
        $campaignEntity = Campaign::load($campaign->id());
        // Get the View for this Campaign entity object.
        $viewBuilder = $this->entityTypeManager->getViewBuilder($entityType);

        // Build the Campaign array to render.
        foreach ($fields as $field_name) {
          if ($campaignEntity->hasField($field_name) && $campaignEntity->access('view')) {
            $value = $campaignEntity->get($field_name);
            $output = $viewBuilder->viewField($value, $viewDisplay);
            $output['#cache']['tags'] = $campaignEntity->getCacheTags();
            $campaignBadge[$field_name] = $output;
          }
        }

        // Get the campaign entity.
        // Build the Campaign object.
        $campaignBadge['donation_frequency_allowed'] = $campaignEntity->getFrequencyAllowed();
        $campaignBadge['getNumberSupporters'] = $campaignEntity->getNumberSupporters();
        $campaignBadge['getPercentage'] = $campaignEntity->getPercentage();
        $campaignBadge['getDaysLeft'] = $campaignEntity->getDaysLeft();
        $campaignBadge['getCampaignStatus'] = $campaignEntity->getCampaignStatus();
        $campaignBadge['one_time_donation_goal'] = (int) $campaignEntity->getGoal();
        $campaignBadge['monthly_donation_goal'] = (int) $campaignEntity->getGoalSustaining();
        $campaignBadge['percentage'] = $campaignEntity->getPercentage();
        $campaignBadge['goal_saved'] = $campaignEntity->getRaisedAmount();
        $campaignBadge['goal'] = $campaignEntity->getGoal();
        // Get currency from plugin.
        $config = $this->configFactory->get('campaign_kit.settings');
        $clientPlugin = $config->get('campaign_kit');
        $campaignBadge['currency'] = $clientPlugin['currency'];

        // Donate link.
        // campaign_kit_core.campaign_donate.
        // Check if Campaign is OPEN.
        if ($campaignEntity->isCampaignOpen()) {
          $donationUrl = Url::fromRoute('campaign_kit_core.campaign_donate', ['campaign' => $campaign->id()]);
          $donationLink = Link::fromTextAndUrl($this->t('Donate Now'), $donationUrl);
          $campaignArray['donate_link'] = $donationLink->toRenderable();
          $campaignArray['donate_link']['#attributes'] = ['class' => ['donate-button']];
        }
        else {
          $closedMessage = $this->t('This campaign is closed');
          $campaignArray['donate_link']['#markup'] = '<label>'. $closedMessage . '</label>';
          $campaignArray['donate_link']['#attributes'] = ['class' => ['donate-button']];
        }

      }
      else {
        $build['#campaign_message'] = $this->t('This will show the Campaign page once you go to the view page.');
        $build['#campaign_message_warning'] = $this->t('This component will be shown only in Campaign entity pages.');
      }
    }
    else {
      $this->messenger->addError($this->t("Unable to find campaign."));
    }

    $build['#badge'] = $campaignBadge;

    return $build;
  }

  /**
   * Get all campaigns in a select field.
   *
   * @return array
   */
  public function getCampaigns() {
    $response = [];

    // Standalone campaigns.
    $connection = \Drupal::getContainer()->get('database');
    $queryStandalone = NULL;
    $queryStandalone = $connection->select('campaign_field_data', 'cfd')->fields('cfd', ['id', 'title']);
    $queryStandalone->condition('campaign_status', 'open', '=');
    $queryStandalone->condition('type', 'standalone', '=');
    $resultStandalone = $queryStandalone->execute();
    $recordsStandalone = $resultStandalone->fetchAll();

    foreach ($recordsStandalone as $key => $campaign) {
      $response['Standalone'][$campaign->id] = $campaign->title;
    }

    // Parent campaigns.
    $queryParent = NULL;
    $queryParent = $connection->select('campaign_field_data', 'cfd')->fields('cfd', ['id', 'title']);
    $queryParent->condition('campaign_status', 'open', '=');
    $queryParent->condition('type', 'parent', '=');
    $resultParent = $queryParent->execute();
    $recordsParent = $resultParent->fetchAll();

    foreach ($recordsParent as $key => $campaign) {
      $response['Parent'][$campaign->id] = $campaign->title;
    }

    // Child campaigns.
    $queryChild = NULL;
    $queryChild = $connection->select('campaign_field_data', 'cfd')->fields('cfd', ['id', 'title']);
    $queryChild->condition('campaign_status', 'open', '=');
    $queryChild->condition('type', 'child', '=');
    $resultChild = $queryChild->execute();
    $recordsChild = $resultChild->fetchAll();

    foreach ($recordsChild as $key => $campaign) {
      $response['Child'][$campaign->id] = $campaign->title;
    }

    return $response;
  }

}
