<?php

namespace Drupal\campaign_kit\Form;

use Drupal\campaign_kit\Campaign\CampaignPayment;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The Configuration Form.
 */
class ConfigurationForm extends FormBase {

  /**
   * The Campaign Payment Service.
   *
   * @var \Drupal\campaign_kit\Campaign\CampaignPayment
   */
  protected $campaignPaymentService;

  /**
   * The Messenger Service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * ConfigurationForm constructor.
   *
   * @param \Drupal\campaign_kit\Campaign\CampaignPayment $campaignService
   *   The Campaign Payment service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Messenger Service.
   */
  public function __construct(CampaignPayment $campaignService, MessengerInterface $messenger) {
    $this->campaign_service = $campaignService;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $campaignService = $container->get('campaign_kit.campaign_payment');
    $messenger = $container->get('messenger');

    return new static(
      $campaignService,
      $messenger
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'payment_processor_form';
  }

  /**
   * Campaign Kit processor form.
   *
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // TODO: Load the configuration of the payment plugin using the machine name.
    $campaignKitConfig = $this->configFactory()->get('campaign_kit.settings');
    $configurationForm = $campaignKitConfig->getRawData();

    // Campaign Kit Notification email.
    $form['campaign_kit_notification_email'] = [
      '#type' => 'email',
      '#title' => $this->t('Notification Email'),
      '#default_value' => $configurationForm['campaign_kit']['notification_email'],
    ];

    // Currency.
    $form['currency'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Currency'),
      '#maxlength' => 6,
      '#size' => 6,
      '#default_value' => $configurationForm['campaign_kit']['currency'],
    ];

    // Campaigns Frequency allowed.
    $form['frequency'] = [
      '#type' => 'select',
      '#title' => $this->t('Campaigns Frequency allowed'),
      '#default_value' => ($configurationForm['campaign_kit']['frequency'] != 'none' ? $configurationForm['campaign_kit']['frequency'] : 'empty_option'),
      '#options' => [
        'onetime' => $this->t('One-time'),
        'monthly' => $this->t('Monthly'),
        'both' => $this->t('Both'),
      ],
      '#empty_option' => $this->t('- None -'),
    ];

    $form['debug_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug mode'),
      '#default_value' => $configurationForm['campaign_kit']['debug_mode'],
    ];

    // Non Campaign Donation Frequency allowed.
    $form['donation_frequency'] = [
      '#type' => 'select',
      '#title' => $this->t('Non-Campaign Donation Frequency allowed'),
      '#default_value' => ($configurationForm['campaign_kit']['donation_frequency'] != 'none' ? $configurationForm['campaign_kit']['donation_frequency'] : 'empty_option'),
      '#options' => [
        'onetime' => $this->t('One-time'),
        'monthly' => $this->t('Monthly'),
        'both' => $this->t('Both'),
      ],
    ];

    $form['image_location'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Image Location'),
      '#description' => $this->t('Set location where Image component stores files under the public folder. For example, "my_custom_folder" may store images in /sites/default/files/my_custom_folder.
'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $configurationForm['campaign_kit']['image_location'],
    ];

    $form['image_extensions'] = [
      '#type' => 'textfield',
      '#title' => $this->t(' Image Component Allowed File Extensions'),
      '#description' => $this->t('Add extensions separated by spaces.'),
      '#required' => TRUE,
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $configurationForm['campaign_kit']['image_extensions'],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * Saves configuration.
   *
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // TODO: Save the configuration using the machine name of the payment plugin.
    $config = $this->configFactory()->getEditable('campaign_kit.settings');
    $config->set('campaign_kit.notification_email', $form_state->getValue('campaign_kit_notification_email'));
    $config->set('campaign_kit.currency', $form_state->getValue('currency'));
    $config->set('campaign_kit.frequency', $form_state->getValue('frequency'));
    $config->set('campaign_kit.debug_mode', $form_state->getValue('debug_mode'));
    // Single Donation Frequency Allowed.
    $config->set('campaign_kit.donation_frequency', $form_state->getValue('donation_frequency'));
    $config->set('campaign_kit.image_location', $form_state->getValue('image_location'));
    $config->set('campaign_kit.image_extensions', $form_state->getValue('image_extensions'));
    $config->save(TRUE);

    $this->messenger->addMessage($this->t('Configuration saved.'));
  }

}
