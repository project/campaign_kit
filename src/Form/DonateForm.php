<?php

namespace Drupal\campaign_kit\Form;

use Drupal\campaign_kit\Campaign\CampaignAPI;
use Drupal\campaign_kit\Campaign\CampaignPayment;
use Drupal\campaign_kit\Donate\CampaignDonate;
use Drupal\campaign_kit_core\Entity\Campaign;
use Drupal\campaign_kit_core\Entity\Donation;
use Drupal\campaign_kit_core\Entity\Team;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Locale\CountryManager;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\path_alias\AliasManager;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\payment\Entity\Payment;
use Drupal\payment\Plugin\Payment\LineItem\PaymentLineItemManager;
use Drupal\payment\Plugin\Payment\Method\PaymentMethodManager;
use Drupal\payment\Plugin\Payment\Type\PaymentTypeManager;
use Drupal\payment_stripe\Event\StripeEvents;
use Drupal\payment_stripe\Event\StripeWebhookChargeFailed;
use Drupal\payment_stripe\Event\StripeWebhookChargeSucceeded;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * The Donate Form.
 */
class DonateForm extends FormBase {

  /**
   * The Event Dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The Campaign service.
   *
   * @var \Drupal\campaign_kit\Campaign\CampaignPayment
   */
  protected $campaignService;

  /**
   * The path alias manager.
   *
   * @var \Drupal\Core\Path\AliasManager
   */
  protected $pathAliasManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The service logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerChannelFactory;

  /**
   * The payment method manager.
   *
   * @var \Drupal\payment\Plugin\Payment\Method\PaymentMethodManager
   */
  protected $paymentMethodManager;

  /**
   * The payment type manager.
   *
   * @var \Drupal\payment\Plugin\Payment\Type\PaymentTypeManager
   */
  protected $paymentTypeManager;

  /**
   * The payment line item manager.
   *
   * @var \Drupal\payment\Plugin\Payment\LineItem\PaymentLineItemManager
   */
  protected $paymentLineItemManager;

  /**
   * The Campaign Donate service.
   *
   * @var \Drupal\campaign_kit\Donate\CampaignDonate
   */
  protected $campaignDonateService;

  /**
   * The Campaign API Service.
   *
   * @var \Drupal\campaign_kit\Campaign\CampaignAPI
   */
  protected $campaignApiService;

  /**
   * The Messenger Service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Routing\CurrentRouteMatch service.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * DonateForm constructor.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The Event Dispatcher.
   * @param \Drupal\campaign_kit\Campaign\CampaignPayment $campaignService
   *   The Campaign custom service.
   * @param \Drupal\Core\Path\AliasManager $pathAliasManager
   *   The path alias manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $loggerChannelFactory
   *   The service logger factory.
   * @param \Drupal\payment\Plugin\Payment\Method\PaymentMethodManager $paymentMethodManager
   *   The payment method manager.
   * @param \Drupal\payment\Plugin\Payment\Type\PaymentTypeManager $paymentTypeManager
   *   The payment type manager.
   * @param \Drupal\payment\Plugin\Payment\LineItem\PaymentLineItemManager $paymentLineItemManager
   *   The line item manager.
   * @param \Drupal\campaign_kit\Donate\CampaignDonate $campaignDonateService
   *   The Campaign Donate service.
   * @param \Drupal\campaign_kit\Campaign\CampaignAPI $campaignApiService
   *   The Campaign API Service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Messenger Service.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $currentRouteMatch
   *   The Current Route Match service.
   */
  public function __construct(EventDispatcherInterface $eventDispatcher, CampaignPayment $campaignService, AliasManager $pathAliasManager, EntityTypeManagerInterface $entityTypeManager, ConfigFactory $configFactory, AccountProxyInterface $currentUser, LoggerChannelFactory $loggerChannelFactory, PaymentMethodManager $paymentMethodManager, PaymentTypeManager $paymentTypeManager, PaymentLineItemManager $paymentLineItemManager, CampaignDonate $campaignDonateService, CampaignAPI $campaignApiService, MessengerInterface $messenger, CurrentRouteMatch $currentRouteMatch) {
    $this->entityTypeManager = $entityTypeManager;
    $this->eventDispatcher = $eventDispatcher;
    $this->campaignService = $campaignService;
    $this->pathAliasManager = $pathAliasManager;
    $this->configFactory = $configFactory;
    $this->currentUser = $currentUser;
    $this->loggerChannelFactory = $loggerChannelFactory;
    $this->paymentMethodManager = $paymentMethodManager;
    $this->paymentTypeManager = $paymentTypeManager;
    $this->paymentLineItemManager = $paymentLineItemManager;
    $this->campaignDonateService = $campaignDonateService;
    $this->campaignApiService = $campaignApiService;
    $this->messenger = $messenger;
    $this->currentRouteMatch = $currentRouteMatch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $eventDispatcher = $container->get('event_dispatcher');
    $campaignService = $container->get('campaign_kit.campaign_payment');
    $pathAliasManager = $container->get('path_alias.manager');
    $entityTypeManager = $container->get('entity_type.manager');
    $configFactory = $container->get('config.factory');
    $currentUser = $container->get('current_user');
    $loggerChannelFactory = $container->get('logger.factory');
    $paymentMethodManager = $container->get('plugin.manager.payment.method');
    $paymentTypeManager = $container->get('plugin.manager.payment.type');
    $paymentLineItemManager = $container->get('plugin.manager.payment.line_item');
    $campaignDonateService = $container->get('campaign_kit.campaign_donate');
    $campaignApiService = $container->get('campaign_kit.campaign_api');
    $messenger = $container->get('messenger');
    $currentRouteMatch = $container->get('current_route_match');

    return new static(
      $eventDispatcher,
      $campaignService,
      $pathAliasManager,
      $entityTypeManager,
      $configFactory,
      $currentUser,
      $loggerChannelFactory,
      $paymentMethodManager,
      $paymentTypeManager,
      $paymentLineItemManager,
      $campaignDonateService,
      $campaignApiService,
      $messenger,
      $currentRouteMatch
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'donate_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $thankYouUrl = NULL, $donationFailedUrl = NULL, $nodeId = NULL) {
    $campaigns = FALSE;
    $campaignId = NULL;
    $campaignLabel = '';
    $campaignStatus = '';
    $campaignFrequency = '';
    $currentCampaignId = '';
    $defaultFrequency = '';
    $singleDonation = FALSE;

    // Get Campaign Entity from route.
    $campaignObject = $this->currentRouteMatch->getParameters()->get('campaign');
    if (isset($campaignObject)) {
      if ($campaignObject instanceof Campaign) {
        $campaignId = $campaignObject->id();

        if ($campaignId) {
          // Using \Drupal::entityTypeManager().
          $campaignStorage = $this->entityTypeManager->getStorage('campaign');
          $campaignStorage->resetCache([$campaignId]);

          /* @var $campaign \Drupal\campaign_kit_core\Entity\Campaign */
          $currentCampaign = $campaignStorage->load($campaignId);
          $campaignStatus = $currentCampaign->get('campaign_status')->value;
          $currentCampaignId = $campaignId;
          // Set if it is working with campaigns.
          $campaigns = TRUE;
        }
      }
    }
    else {
      $singleDonation = TRUE;
    }

    // Get currency from plugin.
    $config = $this->configFactory->get('campaign_kit.settings');
    $clientPlugin = $config->get('campaign_kit');
    $clientPlugin['currency'];

    // Get Country list.
    $countryList = CountryManager::getStandardList();
    $countries = [];
    foreach ($countryList as $key => $value) {
      $countries[$key] = (string) $value;
    }

    // Load Campaign.
    /* @var $currentCampaign \Drupal\campaign_kit_core\Entity\Campaign */
    // $currentCampaign = Campaign::load($currentCampaignId);
    $teamList = [];
    $frequency = [];
    // Only for campaigns.
    if ($campaignStatus == "open" || $singleDonation) {
      // Get Team list.
      $teamStorage = $this->entityTypeManager->getStorage('team');
      $teams = $teamStorage->loadMultiple();
      foreach ($teams as $key => $team) {
        if ($team instanceof Team) {
          if ($team->getAvailable() && $team->get('campaign_id')->target_id == $currentCampaignId) {
            $teamList[$team->id()] = $team->getName();
          }
        }
      }
      asort($teamList);

      // Get entity's frequency.
      if ($campaignStatus == "open") {
        $campaignFrequency = $currentCampaign->getFrequencyAllowed();
        $campaignLabel = $currentCampaign->label();
      }
      else {
        $campaignFrequency = $clientPlugin['donation_frequency'];;
      }
      switch ($campaignFrequency) {
        case 'onetime':
          $frequency = [
            'onetime' => $this->t('One-time'),
          ];
          $defaultFrequency = 'onetime';
          break;

        case 'monthly':
          $frequency = [
            'monthly' => $this->t('Monthly'),
          ];
          $defaultFrequency = 'monthly';
          break;

        case 'both':
          $frequency = [
            'onetime' => $this->t('One-time'),
            'monthly' => $this->t('Monthly'),
          ];
          $defaultFrequency = 'onetime';
          break;
      }

    }

    // Check for both, campaigns and single donation.
    if ($campaigns || $singleDonation) {

      if ($thankYouUrl) {
        $form['thank_you_url'] = [
          '#type' => 'hidden',
          '#value' => $thankYouUrl,
        ];
      }

      if ($donationFailedUrl) {
        $form['donation_failed_url'] = [
          '#type' => 'hidden',
          '#value' => $donationFailedUrl,
        ];
      }

      if ($nodeId) {
        $form['node_id'] = [
          '#type' => 'hidden',
          '#value' => $nodeId,
        ];
      }

      if ($campaigns) {
        $form['campaign_id'] = [
          '#type' => 'hidden',
          '#value' => $currentCampaignId,
        ];
      }

      $form['campaigns'] = [
        '#type' => 'hidden',
        '#value' => $campaigns,
      ];

      // Donation fields.
      $form['amount'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Select amount'),
      ];

      // TODO: Ask Andre about this.
      $form['amount']['lc'] = [
        '#type' => 'hidden',
        '#value' => 'US',
      ];

      $form['amount']['currency_code'] = [
        '#type' => 'hidden',
        '#value' => $clientPlugin['currency'],
      ];

      // TODO: what was this for?
      $form['amount']['test_item'] = [
        '#type' => 'hidden',
        '#value' => $campaignLabel?: 'test_item',
      ];

      $form['amount']['txn_id'] = [
        '#type' => 'hidden',
        '#value' => 'txn_id',
      ];

      $form['amount']['settings']['donation'] = [
        '#type' => 'radios',
        '#default_value' => 'other',
        '#options' => [
          '25.00' => $this->t('$25'),
          '50.00' => $this->t('$50'),
          '100.00' => $this->t('$100'),
          '500.00' => $this->t('$500'),
          'other' => $this->t('Other'),
        ],
        '#prefix' => '<div class="donation-radios--amount">',
        '#suffix' => '</div>',
      ];

      $form['amount']['amount'] = [
        '#type' => 'number',
        '#prefix' => '<div class="donation-data-amount">',
        '#title' => $this->t('Your donation'),
        '#required' => TRUE,
        '#field_suffix' => $clientPlugin['currency'],
        '#attributes' => [
          'min' => '0',
          'max' => '999999',
        ],
      ];

      // TODO: Update this with plugin name of new config.
      $form['amount']['recurrence']['time'] = [
        '#type' => 'radios',
        '#default_value' => $defaultFrequency,
        '#options' => $frequency,
        '#attributes' => [
          'required' => TRUE,
        ],
        '#suffix' => '</div>',
      ];

      $form['amount']['dedication']['dedicate_donation'] = [
        '#type' => 'checkboxes',
        '#options' => ['1' => $this->t('Make my donation in honor or in memory of someone')],
      ];

      $form['dedication-information'] = [
        '#type' => 'fieldset',
      ];

      $form['dedication-information']['dedication-type']['dedicate_type'] = [
        '#type' => 'radios',
        '#default_value' => 'in_honor_of',
        '#options' => [
          'in_honor_of' => $this->t('In honor of...'),
          'in_memory_of' => $this->t('In memory of...'),
        ],
        '#prefix' => '<div class="edit-dedicate--type">',
        '#suffix' => '</div>',
      ];

      $form['dedication-information']['honoree_first_name'] = [
        '#type' => 'textfield',
        '#prefix' => '<div class="data-dedication-info">',
        '#title' => $this->t("Honoree's Name"),
        '#maxlength' => '29',
        '#attributes' => ['placeholder' => $this->t('First Name')],
      ];

      $form['dedication-information']['honoree_last_name'] = [
        '#type' => 'textfield',
        '#suffix' => '</div>',
        '#title' => $this->t("Honoree's Last Name"),
        '#maxlength' => '29',
        '#attributes' => ['placeholder' => $this->t('Last Name')],
      ];

      $form['dedication-information']['recipient_mail'] = [
        '#type' => 'email',
        '#title' => $this->t("Recipient’s Email"),
        '#maxlength' => '60',
        '#description' => $this->t('Optionally send a notification email'),
        '#attributes' => ['placeholder' => $this->t('Email Address')],
      ];

      $form['dedication-information']['recipient_first_name'] = [
        '#type' => 'textfield',
        '#prefix' => '<div class="data-dedication-info">',
        '#title' => $this->t("Recipient's Name"),
        '#maxlength' => '30',
        '#attributes' => ['placeholder' => $this->t('First Name')],
      ];

      $form['dedication-information']['recipient_last_name'] = [
        '#type' => 'textfield',
        '#suffix' => '</div>',
        '#title' => $this->t("Recipient's Last Name"),
        '#maxlength' => '30',
        '#attributes' => ['placeholder' => $this->t('Last Name')],
      ];

      $form['dedication-information']['recipient_message'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Message to the Recipient'),
        '#description' => $this->t('Max. 250 characters.'),
        '#attributes' => [
          'maxlength' => ['250'],
        ],
        '#default_value' => $this->t('I am making this donation in honor of....'),
      ];

      if ($teamList) {
        $form['team-information'] = [
          '#type' => 'fieldset',
          '#title' => $this->t('Team'),
        ];

        $form['team-information']['team_id'] = [
          '#type' => 'select',
          '#title' => $this->t('Team'),
          '#options' => $teamList,
          '#empty_option' => $this->t('--No team--'),
        ];
      }

      $form['donor-information'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Your information'),
      ];

      $form['donor-information']['title'] = [
        '#type' => 'select',
        '#title' => $this->t('Title'),
        '#options' => [
          'Mr.' => $this->t('Mr.'),
          'Ms.' => $this->t('Ms.'),
          'Mrs.' => $this->t('Mrs.'),
          'Miss' => $this->t('Miss'),
          'Dr.' => $this->t('Dr.'),
        ],
        '#empty_option' => $this->t('Please select'),
      ];

      $form['donor-information']['first_name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('First Name'),
        '#maxlength' => '29',
        '#required' => TRUE,
      ];

      $form['donor-information']['last_name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Last Name'),
        '#maxlength' => '29',
        '#required' => TRUE,
      ];

      $form['donor-information']['email'] = [
        '#type' => 'email',
        '#title' => $this->t('Email'),
        '#maxlength' => '60',
        '#required' => TRUE,
        '#field_suffix' => $this->t('Your receipt will be emailed here.'),
      ];

      $form['donor-information']['comment'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Leave a comment'),
        '#field_suffix' => $this->t('Max. 100 characters .'),
        '#maxlength' => '100',
      ];

      $form['payment-details'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Payment Details'),
      ];

      $form['payment-details']['name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Name'),
        '#maxlength' => '50',
        '#required' => TRUE,
      ];

      $form['payment-details']['stripe'] = [
        '#type' => 'stripe',
        '#title' => $this->t('Credit card'),
        '#required' => TRUE,
        // The selectors are gonna be looked within the enclosing form only.
        "#stripe_selectors" => [],
      ];

      $form['payment-details']['address1'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Address'),
        '#maxlength' => '50',
        '#required' => TRUE,
      ];

      $form['payment-details']['address2'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Address Line 2'),
        '#maxlength' => '50',
      ];

      $form['payment-details']['country'] = [
        '#type' => 'select',
        '#title' => $this->t('Country'),
        '#maxlength' => '50',
        '#options' => $countries,
        '#default_value' => 'US',
        '#required' => TRUE,
        '#empty_option' => $this->t('Please select'),
        '#prefix' => '<div id="country-output">',
        '#suffix' => '</div>',
      ];

      $form['payment-details']['city'] = [
        '#type' => 'textfield',
        '#title' => $this->t('City'),
        '#maxlength' => '50',
        '#required' => TRUE,
      ];

      $form['payment-details']['state_province_us'] = [
        '#type' => 'select',
        '#title' => $this->t('State/Province'),
        '#maxlength' => '50',
        '#options' => $this->campaignApiService->getUSStates(),
        //'#required' => TRUE,
        '#empty_option' => $this->t('Please select'),
        '#prefix' => '<div id="us-output">',
        '#suffix' => '</div>',
      ];

      $form['payment-details']['state_province_ca'] = [
        '#type' => 'select',
        '#title' => $this->t('State/Province'),
        '#maxlength' => '50',
        '#options' => $this->campaignApiService->getCanadaStates(),
        //'#required' => TRUE,
        '#empty_option' => $this->t('Please select'),
        '#prefix' => '<div id="ca-output">',
        '#suffix' => '</div>',
      ];

      $form['payment-details']['state_province_any'] = [
        '#type' => 'textfield',
        '#title' => $this->t('State/Province'),
        //'#required' => TRUE,
        '#size' => '60',
        '#prefix' => '<div id="any-output">',
        '#suffix' => '</div>',
      ];

      $form['payment-details']['zip'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Postal code'),
        '#maxlength' => '12',
        '#required' => TRUE,
      ];

      $form['actions']['#type'] = 'actions';
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Send your gift'),
        '#button_type' => 'primary',
      ];
      return $form;
    }
    else {
      $this->messenger->addMessage($this->t('Campaign complete.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $workWithCampaigns = $form_state->getValue('campaigns');
    if ($workWithCampaigns) {
      $currentCampaign = $form_state->getValue('campaign_id');

      $campaignStorage = $this->entityTypeManager->getStorage('campaign');
      $campaignStorage->resetCache([$currentCampaign]);

      /* @var $campaign \Drupal\campaign_kit_core\Entity\Campaign */
      $campaign = $campaignStorage->load($currentCampaign);
      $campaignStatus = $campaign->get('campaign_status')->value;

      if ($campaignStatus != 'open') {
        $form_state->setErrorByName('campaign_id', $this->t('Sorry! This campaign is now complete.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get the current user.
    $userId = $this->currentUser->id();
    $config = $this->configFactory->get('stripe.settings');

    $workWithCampaigns = $form_state->getValue('campaigns');
    $stripeToken = $form_state->getValue('stripe');
    $currency = $form_state->getValue('currency_code');
    $campaignId = $form_state->getValue('campaign_id');
    $campaignTitle = $form_state->getValue('test_item');
    $email = $form_state->getValue('email');
    $donationValues = [];

    /*
     *  Stripe settings
     */
    if ($this->campaignDonateService->checkTestStripeApiKey($config)) {
      if ($workWithCampaigns) {
        // Make test charge if we have test environment and api key.
        // Token is created using checkout or form element!
        // Get the payment token ID submitted by the form:
        // $stripeToken = $form_state->getValue('stripe');
        // $currency = $form_state->getValue('currency_code');
        // $campaignId = $form_state->getValue('campaign_id');
        // $campaignTitle = $form_state->getValue('test_item');
        // $email = $form_state->getValue('email');
        // Create charge at Stripe.
        $charge = $this->campaignDonateService->createCharge($config, $stripeToken, $form_state->getValue('amount'), $currency, $campaignId, $campaignTitle, $email);

        // Save Campaign Donation, Campaign Transaction, and Payment entities.
        if ($charge->status == 'succeeded') {
          // Create Campaign Donation entity.
          $campaignDonationValues = [
            'type' => 'campaign_donation',
            'name' => $form_state->getValue('first_name') . ' ' . $form_state->getValue('last_name'),
            'user_id' => [
              'target_id' => $userId,
            ],
            'payment_status' => 'Confirmed',
            'anonymous' => 1,
            'first_name' => $form_state->getValue('first_name'),
            'last_name' => $form_state->getValue('last_name'),
            'mail' => $form_state->getValue('email'),
            'amount' => $form_state->getValue('amount'),
            'campaign_id' => [
              'target_id' => $form_state->getValue('campaign_id'),
            ],
            'payment_txn_id' => $charge->id,
            'currency' => $form_state->getValue('currency_code'),
            'referred_by' => '',
            'title' => $form_state->getValue('title'),
            'opt_in' => 0,
            'payment_processor' => 'payment_processor',
            'dedicate_donation' => $form_state->getValue('dedicate_donation'),
            'dedicate_type' => $form_state->getValue('dedicate_type'),
            'honoree_first_name' => $form_state->getValue('honoree_first_name'),
            'honoree_last_name' => $form_state->getValue('honoree_last_name'),
            'recipient_mail' => $form_state->getValue('recipient_mail'),
            'recipient_first_name' => $form_state->getValue('recipient_first_name'),
            'recipient_last_name' => $form_state->getValue('recipient_last_name'),
            'recipient_message' => [
              'value' => $form_state->getValue('recipient_message'),
              'format' => 'basic_html',
            ],
            'donation_frequency' => $form_state->getValue('time'),
            'team_id' => [
              'target_id' => $form_state->getValue('team_id'),
            ],
          ];
          $campaignDonation = $this->campaignApiService->createCampaignDonation($campaignDonationValues);
          try {
            $campaignDonation->save();
          }
          catch (Exception $e) {
            $message = $this->t("Your transaction did not save. Please contact the administrator.");
            $this->loggerChannelFactory->get('campaign_kit')->error($message);
            $message = $e->getMessage();
            $this->loggerChannelFactory->get('campaign_kit')->error($message);
          }
          $campaignDonationId = $campaignDonation->id();

          $state = $this->getStateProvince($form_state->getValue('country'));

          // Create Campaign Transaction entity.
          $campaignTransactionValues = [
            'type' => 'campaign_transaction',
            'user_id' => [
              'target_id' => $userId,
            ],
            'name' => $form_state->getValue('name'),
            'address_1' => $form_state->getValue('address1'),
            'address_2' => $form_state->getValue('address2'),
            'city' => $form_state->getValue('city'),
            'country' => $form_state->getValue('country'),
            'state_province' => $form_state->getValue($state),
            'postal_code' => $form_state->getValue('zip'),
            'campaign_id' => [
              'target_id' => $form_state->getValue('campaign_id'),
            ],
          ];
          $campaignTransaction = $this->campaignApiService->createCampaignTransaction($campaignTransactionValues);
          try {
            $campaignTransaction->save();
          }
          catch (Exception $e) {
            $message = $this->t("Your transaction did not save. Please contact the administrator.");
            $this->loggerChannelFactory->get('campaign_kit')->error($message);
            $message = $e->getMessage();
            $this->loggerChannelFactory->get('campaign_kit')->error($message);
          }

          // TODO: Load Campaign and set One-time and Monthly total amounts.
          $campaign = Campaign::load($form_state->getValue('campaign_id'));
          if ($campaign instanceof Campaign) {
            $campaign->set('total_one_time_amount', (int) $campaign->getRaisedAmountOneTime());
            $campaign->set('total_monthly_amount', (int) $campaign->getRaisedAmountMonthly());
            $campaign->save();
          }

          // Saving a Payment.
          // You need to pass the bundle when you create a payment.
          /* @var \Drupal\payment\Entity\Payment */
          $payment = Payment::create([
            'bundle' => 'campaign',
            /*
             * 'payment_id' => $charge->id,
             * 'campaign_id' => 1,
             */
          ]);

          // Set up my custom payment method.
          $paymentMethodManager = $this->paymentMethodManager;
          $payment->setPaymentMethod($paymentMethodManager->createInstance($this->campaignService->getPaymentMethod()));

          // Set custom payment type.
          $paymentTypeManager = $this->paymentTypeManager;
          $payment->setPaymentType($paymentTypeManager->createInstance($this->campaignService->getPaymentType()));

          // Viewing the payment page would cause a fatal error in
          // \Drupal\payment\Plugin\views\field\PaymentAmount if not set.
          // Also make sure you have imported/enabled the currency via
          // the UI or it will be saved as null.
          $payment->setCurrencyCode($form_state->getValue('currency_code'));

          // Create a Line Item instance.
          $lineItemManager = $this->paymentLineItemManager;
          $line_item = $lineItemManager->createInstance('payment_basic')
            ->setDescription($form_state->getValue('test_item'))
            ->setQuantity(1)
            ->setAmount($form_state->getValue('amount'))
            ->setName($form_state->getValue('test_item'))
            ->setCurrencyCode($form_state->getValue('currency_code'));
          $payment->setLineItems([$line_item]);
          $payment->execute();

          /*
          // Fires Succeed event.
          $chargeSucceeded = new StripeWebhookChargeSucceeded($charge);
          $this->eventDispatcher->dispatch(StripeEvents::WEBHOOK_CHARGE_SUCCEEDED, $chargeSucceeded);
          */

          // Redirect user.
          $form_state->setRedirect('campaign_kit_core.campaign_thank_you', [
            'campaign' => $form_state->getValue('campaign_id'),
            'campaignDonationId' => $campaignDonationId,
          ]);
        }
        elseif ($charge->status == 'failed') {
          /*
          // TODO: Fires Failed event, re think both.
          $chargeFailed = new StripeWebhookChargeFailed($charge);
          $this->eventDispatcher->dispatch(StripeEvents::WEBHOOK_CHARGE_SUCCEEDED, $chargeFailed);
          */
          $messageError = $this->t("Payment failed. Please contact the site owner.");
          $this->messenger->addError($messageError);
        }

        // TODO: Campaign Kit after saving the donation and when Stripe Payment
        // TODO: sends Charge:succeed or Charge:failed, we need to update both events.
        // TODO: DB and show a message.
      }
      else {
        // Create charge at Stripe.
        $charge = $this->campaignDonateService->createCharge($config, $stripeToken, $form_state->getValue('amount'), $currency, $campaignId, $campaignTitle, $email);

        if ($charge->status == 'succeeded') {
          // Datetime.
          /* @var \Drupal\Core\Datetime\DrupalDateTime $datetimeStart */
          $datetimeDonation = new DrupalDateTime();
          $datetimeDonationString = $datetimeDonation->format('Y-m-d\TH:i:s');

          $state = $this->getStateProvince($form_state->getValue('country'));

          // Create single donation.
          $donationValues = [
            'type' => 'donation',
            'name' => $form_state->getValue('first_name') . ' ' . $form_state->getValue('last_name'),
            'user_id' => [
              'target_id' => $userId,
            ],
            'payment_status' => 'Confirmed',
            'first_name' => $form_state->getValue('first_name'),
            'last_name' => $form_state->getValue('last_name'),
            'amount' => $form_state->getValue('amount'),
            'donation_frequency' => $form_state->getValue('time'),
            'mail' => $form_state->getValue('email'),
            // 'currency' => $form_state->getValue('currency_code'),.
            'referred_by' => '',
            'title' => $form_state->getValue('title'),
            'opt_in' => 0,
            'payment_processor' => 'payment_processor',
            'dedicate_donation' => $form_state->getValue('dedicate_donation'),
            'dedicate_type' => $form_state->getValue('dedicate_type'),
            'honoree_first_name' => $form_state->getValue('honoree_first_name'),
            'honoree_last_name' => $form_state->getValue('honoree_last_name'),
            'recipient_mail' => $form_state->getValue('recipient_mail'),
            'recipient_first_name' => $form_state->getValue('recipient_first_name'),
            'recipient_last_name' => $form_state->getValue('recipient_last_name'),
            'recipient_message' => [
              'value' => $form_state->getValue('recipient_message'),
              'format' => 'basic_html',
            ],
            // 'donation_frequency' => $form_state->getValue('time'),.
            'address_1' => $form_state->getValue('address1'),
            'address_2' => $form_state->getValue('address2'),
            'city' => $form_state->getValue('city'),
            'country' => $form_state->getValue('country'),
            'state_province' => $form_state->getValue($state),
            'postal_code' => $form_state->getValue('zip'),
            'date' => $datetimeDonationString,
          ];
          // Check Donation Frequency.
          switch ($form_state->getValue('time')) {
            case 'onetime':
              $donationValues['amount_one_time'] = $form_state->getValue('amount');
              break;

            case 'monthly':
              $donationValues['amount_monthly'] = $form_state->getValue('amount');
              break;
          }

          $donation = Donation::create($donationValues);

          try {
            $donation->save();
          }
          catch (Exception $e) {
            $message = $this->t("Your transaction did not save. Please contact the administrator.");
            $this->loggerChannelFactory->get('campaign_kit')->error($message);
            $message = $e->getMessage();
            $this->loggerChannelFactory->get('campaign_kit')->error($message);
          }

          // Saving a Payment.
          // You need to pass the bundle when you create a payment.
          /* @var \Drupal\payment\Entity\Payment */
          $payment = Payment::create([
            'bundle' => 'campaign',
            /*
             * 'payment_id' => $charge->id,
             * 'campaign_id' => 1,
             */
          ]);

          // Set up my custom payment method.
          $paymentMethodManager = $this->paymentMethodManager;
          $payment->setPaymentMethod($paymentMethodManager->createInstance($this->campaignService->getPaymentMethod()));

          // Set custom payment type.
          $paymentTypeManager = $this->paymentTypeManager;
          $payment->setPaymentType($paymentTypeManager->createInstance($this->campaignService->getPaymentType()));

          // Viewing the payment page would cause a fatal error in
          // \Drupal\payment\Plugin\views\field\PaymentAmount if not set.
          // Also make sure you have imported/enabled the currency via
          // the UI or it will be saved as null.
          $payment->setCurrencyCode($form_state->getValue('currency_code'));

          // Create a Line Item instance.
          $lineItemManager = $this->paymentLineItemManager;
          $line_item = $lineItemManager->createInstance('payment_basic')
            ->setDescription($form_state->getValue('test_item'))
            ->setQuantity(1)
            ->setAmount($form_state->getValue('amount'))
            ->setName($form_state->getValue('test_item'))
            ->setCurrencyCode($form_state->getValue('currency_code'));
          $payment->setLineItems([$line_item]);
          $payment->execute();

          // Redirect to thank you page.
          // Attach donation ID and node ID.
          $response = new TrustedRedirectResponse($form_state->getValue('thank_you_url') . '?donation=' . $donation->id() . '&node=' . $form_state->getValue('node_id'));
          $form_state->setResponse($response);
        }
        elseif ($charge->status == 'failed') {
          // TODO: Fires Failed event, re think both.
          $chargeFailed = new StripeWebhookChargeFailed($charge);
          $this->eventDispatcher->dispatch(StripeEvents::WEBHOOK_CHARGE_SUCCEEDED, $chargeFailed);
          $messageError = $this->t("Payment failed. Please contact the site owner.");
          $this->messenger->addError($messageError);

          // Redirect to failing page.
          $form_state->setRedirectUrl($form_state->getValue('donation_failed_url'));
        }
      }
    }
    else {
      $messageError = $this->t('Please add and configure a payment plugin.');
      $this->messenger->addError($messageError);
    }
  }

  /**
   * Get proper State/Province field.
   *
   * @param $country
   * @return string
   */
  public function getStateProvince($country) {
    switch ($country) {
      case 'US':
        $res = 'state_province_us';
        break;
      case 'CA':
        $res = 'state_province_ca';
        break;
      default:
        $res = 'state_province_any';
    }

    return $res;
  }

}
