== Spell Checking ==

use the cspell module:
https://www.npmjs.com/package/cspell

== Sample Usage ==

Each type of source file has its own config file.
php: campaign_kit/project/cspell-php.json
twig and css: ampaign_kit/project/cspell-twig.json
js: ampaign_kit/project/cspell-js.json

Run from above project directory:
cspell --color --config campaign_kit/project/cspell/cspell-php.json campaign_kit/**/*.php  | less -r
cspell --color --config campaign_kit/project/cspell/cspell-twig.json campaign_kit/**/*.twig campaign_kit/**/*.css | less -r
cspell --color --config campaign_kit/project/cspell/cspell-js.json campaign_kit/**/*.js  | less -r
