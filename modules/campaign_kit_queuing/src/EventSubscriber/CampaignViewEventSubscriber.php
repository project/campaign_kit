<?php

namespace Drupal\campaign_kit_queuing\EventSubscriber;

use Drupal;
use Drupal\campaign_kit_core\Entity\Campaign;
use Drupal\core_event_dispatcher\Event\Entity\EntityViewEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class CampaignViewEventSubscriber.
 *
 * @package Drupal\campaign_kit_queuing
 */
class CampaignViewEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[HookEventDispatcherInterface::ENTITY_VIEW][] = ['alterCampaign'];
    return $events;
  }

  /**
   * Alter Campaign.
   *
   * @param \Drupal\hook_event_dispatcher\Event\Entity\EntityViewEvent $event
   *   The event.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function alterCampaign(EntityViewEvent $event) {
    $campaign = $event->getEntity();
    if ($campaign instanceof Campaign) {
      $complete = $campaign->isCampaignOpen();
      if ($complete) {
        // Flush tag cache for Campaign Entity.
        /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager */
        $entityTypeManager = Drupal::entityTypeManager();
        $campaignStorage = $entityTypeManager->getStorage('campaign');
        $campaignStorage->resetCache([$campaign->id()]);
      }
    }
  }

}
