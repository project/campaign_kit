<?php

/**
 * @file
 * Contains campaign_kit_core.module.
 */

use Drupal\views\ViewExecutable;
use Drupal\campaign_kit_core\Entity\Campaign;
use Drupal\campaign_kit_core\Entity\Team;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Implements hook_help().
 */
function campaign_kit_core_help($routeName) {
  switch ($routeName) {
    // Main module help for the campaign_kit_core module.
    case 'help.page.campaign_kit_core':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('This module creates all the entities to create campaigns.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_theme().
 */
function campaign_kit_core_theme() {
  $theme = [];
  $theme['campaign'] = [
    'render element' => 'elements',
    'file' => 'campaign.page.inc',
    'template' => 'campaign',
  ];
  $theme['campaign_content_add_list'] = [
    'render element' => 'content',
    'variables' => ['content' => NULL],
    'file' => 'campaign.page.inc',
  ];
  $theme['campaign_donation'] = [
    'render element' => 'elements',
    'file' => 'campaign_donation.page.inc',
    'template' => 'campaign_donation',
  ];
  $theme['campaign_donation_content_add_list'] = [
    'render element' => 'content',
    'variables' => ['content' => NULL],
    'file' => 'campaign_donation.page.inc',
  ];
  $theme['campaign_transaction'] = [
    'render element' => 'elements',
    'file' => 'campaign_transaction.page.inc',
    'template' => 'campaign_transaction',
  ];
  $theme['campaign_transaction_content_add_list'] = [
    'render element' => 'content',
    'variables' => ['content' => NULL],
    'file' => 'campaign_transaction.page.inc',
  ];
  $theme['campaign_update'] = [
    'render element' => 'elements',
    'file' => 'campaign_update.page.inc',
    'template' => 'campaign_update',
  ];
  $theme['campaign_update_content_add_list'] = [
    'render element' => 'content',
    'variables' => ['content' => NULL],
    'file' => 'campaign_update.page.inc',
  ];
  $theme['donation'] = [
    'render element' => 'elements',
    'file' => 'donation.page.inc',
    'template' => 'donation',
  ];
  $theme['donation_content_add_list'] = [
    'render element' => 'content',
    'variables' => ['content' => NULL],
    'file' => 'donation.page.inc',
  ];
  $theme['team'] = [
    'render element' => 'elements',
    'file' => 'team.page.inc',
    'template' => 'team',
  ];
  $theme['team_content_add_list'] = [
    'render element' => 'content',
    'variables' => ['content' => NULL],
    'file' => 'team.page.inc',
  ];
  return $theme;
}

/**
 * Implements hook_theme_suggestions_HOOK().
 */
function campaign_kit_core_theme_suggestions_campaign(array $variables) {
  $suggestions = [];
  /* @var $campaign \Drupal\campaign_kit_core\Entity\Campaign */
  $campaign = $variables['elements']['#campaign'];
  $sanitizedViewMode = strtr($variables['elements']['#view_mode'], '.', '_');

  $suggestions[] = 'campaign__' . $sanitizedViewMode;
  $suggestions[] = 'campaign__' . $campaign->bundle();
  $suggestions[] = 'campaign__' . $campaign->bundle() . '__' . $sanitizedViewMode;
  $suggestions[] = 'campaign__' . $campaign->id();
  $suggestions[] = 'campaign__' . $campaign->id() . '__' . $sanitizedViewMode;
  return $suggestions;
}

/**
 * Implements hook_theme_suggestions_HOOK().
 */
function campaign_kit_core_theme_suggestions_campaign_donation(array $variables) {
  $suggestions = [];
  /* @var $campaignDonation \Drupal\campaign_kit_core\Entity\CampaignDonation */
  $campaignDonation = $variables['elements']['#campaign_donation'];
  $sanitizedViewMode = strtr($variables['elements']['#view_mode'], '.', '_');

  $suggestions[] = 'campaign_donation__' . $sanitizedViewMode;
  $suggestions[] = 'campaign_donation__' . $campaignDonation->bundle();
  $suggestions[] = 'campaign_donation__' . $campaignDonation->bundle() . '__' . $sanitizedViewMode;
  $suggestions[] = 'campaign_donation__' . $campaignDonation->id();
  $suggestions[] = 'campaign_donation__' . $campaignDonation->id() . '__' . $sanitizedViewMode;
  return $suggestions;
}

/**
 * Implements hook_theme_suggestions_HOOK().
 */
function campaign_kit_core_theme_suggestions_campaign_transaction(array $variables) {
  $suggestions = [];
  /* @var $campaignTransaction \Drupal\campaign_kit_core\Entity\CampaignTransaction */
  $campaignTransaction = $variables['elements']['#campaign_transaction'];
  $sanitizedViewMode = strtr($variables['elements']['#view_mode'], '.', '_');

  $suggestions[] = 'campaign_transaction__' . $sanitizedViewMode;
  $suggestions[] = 'campaign_transaction__' . $campaignTransaction->bundle();
  $suggestions[] = 'campaign_transaction__' . $campaignTransaction->bundle() . '__' . $sanitizedViewMode;
  $suggestions[] = 'campaign_transaction__' . $campaignTransaction->id();
  $suggestions[] = 'campaign_transaction__' . $campaignTransaction->id() . '__' . $sanitizedViewMode;
  return $suggestions;
}

/**
 * Implements hook_theme_suggestions_HOOK().
 */
function campaign_kit_core_theme_suggestions_campaign_update(array $variables) {
  $suggestions = [];
  /* @var $campaignUpdate \Drupal\campaign_kit_core\Entity\CampaignUpdate */
  $campaignUpdate = $variables['elements']['#campaign_update'];
  $sanitizedViewMode = strtr($variables['elements']['#view_mode'], '.', '_');

  $suggestions[] = 'campaign_update__' . $sanitizedViewMode;
  $suggestions[] = 'campaign_update__' . $campaignUpdate->bundle();
  $suggestions[] = 'campaign_update__' . $campaignUpdate->bundle() . '__' . $sanitizedViewMode;
  $suggestions[] = 'campaign_update__' . $campaignUpdate->id();
  $suggestions[] = 'campaign_update__' . $campaignUpdate->id() . '__' . $sanitizedViewMode;
  return $suggestions;
}

/**
 * Implements hook_theme_suggestions_HOOK().
 */
function campaign_kit_core_theme_suggestions_donation(array $variables) {
  $suggestions = [];
  /* @var $donation \Drupal\campaign_kit_core\Entity\Donation */
  $donation = $variables['elements']['#donation'];
  $sanitizedViewMode = strtr($variables['elements']['#view_mode'], '.', '_');

  $suggestions[] = 'donation__' . $sanitizedViewMode;
  $suggestions[] = 'donation__' . $donation->bundle();
  $suggestions[] = 'donation__' . $donation->bundle() . '__' . $sanitizedViewMode;
  $suggestions[] = 'donation__' . $donation->id();
  $suggestions[] = 'donation__' . $donation->id() . '__' . $sanitizedViewMode;
  return $suggestions;
}

/**
 * Implements hook_entity_type_build().
 */
function campaign_kit_core_entity_type_build(array &$entity_types) {
  // This is for /campaign/{campaignId}/child/add form.
  $entity_types['campaign']->setFormClass('standalone', 'Drupal\campaign_kit_core\Form\CampaignForm');
  $entity_types['campaign']->setFormClass('parent', 'Drupal\campaign_kit_core\Form\CampaignForm');
  $entity_types['campaign']->setFormClass('child', 'Drupal\campaign_kit_core\Form\CampaignForm');
}

/**
 * Implements hook_theme_suggestions_HOOK().
 */
function campaign_kit_core_theme_suggestions_team(array $variables) {
  $suggestions = [];
  /* @var $entity \Drupal\campaign_kit_core\Entity\Team */
  $entity = $variables['elements']['#team'];
  $sanitized_view_mode = strtr($variables['elements']['#view_mode'], '.', '_');

  $suggestions[] = 'team__' . $sanitized_view_mode;
  $suggestions[] = 'team__' . $entity->bundle();
  $suggestions[] = 'team__' . $entity->bundle() . '__' . $sanitized_view_mode;
  $suggestions[] = 'team__' . $entity->id();
  $suggestions[] = 'team__' . $entity->id() . '__' . $sanitized_view_mode;
  return $suggestions;
}

/**
 * Implements hook_entity_operation_alter().
 */
function campaign_kit_core_entity_operation_alter(array &$operations, EntityInterface $entity) {
  // Add Leaderboard link.
  if ($entity instanceof Campaign) {
    $operations['leaderboard'] = [
      'title' => t('Leaderboard'),
      'weight' => 101,
      'url' => Url::fromRoute("campaign_kit.ledearboard_page", [
        $entity->getEntityTypeId() => $entity->id(),
      ]),
    ];
  }

  return $operations;
}

/**
 * Computed field_total_monthly.
 *
 * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
 *   The Entity Type Manager service.
 * @param \Drupal\campaign_kit_core\Entity\Campaign $entity
 *   The entity to be processed.
 * @param array $fields
 *   The available fields.
 * @param string $delta
 *   The index when computed field is being used more than once.
 *
 * @return string
 *   Returns $value.
 */
function computed_field_field_total_monthly_compute($entity_type_manager, $entity, $fields, $delta) {
  $totalMonthly = '';
  if ($entity instanceof Campaign) {
    $totalMonthly = '$' . $entity->getRaisedAmountMonthly();
  }
  return $value = $totalMonthly;
}

/**
 * Computed field_total_one_time.
 *
 * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
 *   The Entity Type Manager service.
 * @param \Drupal\campaign_kit_core\Entity\Campaign $entity
 *   The entity to be processed.
 * @param array $fields
 *   The available fields.
 * @param string $delta
 *   The index when computed field is being used more than once.
 *
 * @return string
 *   Returns $value.
 */
function computed_field_field_total_one_time_compute($entity_type_manager, $entity, $fields, $delta) {
  $totalOneTime = '';
  if ($entity instanceof Campaign) {
    $totalOneTime = '$' . $entity->getRaisedAmountOneTime();
  }
  return $value = $totalOneTime;
}

/**
 * Computed field_team_total_one_time.
 *
 * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
 *   The Entity Type Manager service.
 * @param \Drupal\campaign_kit_core\Entity\Team $entity
 *   The entity to be processed.
 * @param array $fields
 *   The available fields.
 * @param string $delta
 *   The index when computed field is being used more than once.
 *
 * @return string
 *   Returns $value.
 */
function computed_field_field_team_total_one_time_compute($entity_type_manager, $entity, $fields, $delta) {
  $currentRoute = \Drupal::routeMatch();
  $campaign = $currentRoute->getParameters()->get('campaign');
  if ($campaign instanceof Campaign) {
    if ($entity instanceof Team) {
      $connection = \Drupal::getContainer()->get('database');
      $query = NULL;
      $query = $connection->select('team_field_data', 'tfd')->fields('tfd', ['id', 'name']);
      $query->addExpression("(SELECT sum(amount) FROM campaign_donation_field_data cdfd2 WHERE cdfd2.campaign_id = :campaign_id AND cdfd2.team_id=tfd.id AND cdfd2.donation_frequency = 'onetime')", 'totalAmountOneTime', [':campaign_id' => $campaign->id()]);
      $query->condition('campaign_id', $campaign->id(), '=');
      $query->condition('id', $entity->id(), '=');
      $result = $query->execute();
      $records = $result->fetchAll();
    }
  }

  return $value = $records[0]->totalAmountOneTime ? '$' . $records[0]->totalAmountOneTime : '';
}

/**
 * Computed field_team_total_monthly.
 *
 * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
 *   The Entity Type Manager service.
 * @param \Drupal\campaign_kit_core\Entity\Team $entity
 *   The entity to be processed.
 * @param array $fields
 *   The available fields.
 * @param string $delta
 *   The index when computed field is being used more than once.
 *
 * @return string
 *   Returns $value.
 */
function computed_field_field_team_total_monthly_compute($entity_type_manager, $entity, $fields, $delta) {
  $currentRoute = \Drupal::routeMatch();
  $campaign = $currentRoute->getParameters()->get('campaign');
  if ($campaign instanceof Campaign) {
    if ($entity instanceof Team) {
      $connection = \Drupal::getContainer()->get('database');
      $query = NULL;
      $query = $connection->select('team_field_data', 'tfd')->fields('tfd', ['id', 'name']);
      $query->addExpression("(SELECT sum(amount) FROM campaign_donation_field_data cdfd4 WHERE cdfd4.campaign_id = :campaign_id AND cdfd4.team_id=tfd.id AND cdfd4.donation_frequency = 'monthly')", 'totalAmountMonthly', [':campaign_id' => $campaign->id()]);
      $query->condition('campaign_id', $campaign->id(), '=');
      $query->condition('id', $entity->id(), '=');
      $result = $query->execute();
      $records = $result->fetchAll();
    }
  }

  return $value = $records[0]->totalAmountMonthly ? '$' . $records[0]->totalAmountMonthly : '';
}

/**
 * Computed field_one_time_donors.
 *
 * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
 *   The Entity Type Manager service.
 * @param \Drupal\campaign_kit_core\Entity\Team $entity
 *   The entity to be processed.
 * @param array $fields
 *   The available fields.
 * @param string $delta
 *   The index when computed field is being used more than once.
 *
 * @return string
 *   Returns $value.
 */
function computed_field_field_one_time_donors_compute($entity_type_manager, $entity, $fields, $delta) {
  $currentRoute = \Drupal::routeMatch();
  $campaign = $currentRoute->getParameters()->get('campaign');
  if ($campaign instanceof Campaign) {
    if ($entity instanceof Team) {
      $connection = \Drupal::getContainer()->get('database');
      $query = NULL;
      $query = $connection->select('team_field_data', 'tfd')->fields('tfd', ['id', 'name']);
      $query->addExpression("(SELECT count(*) FROM campaign_donation_field_data cdfd3 WHERE cdfd3.campaign_id = :campaign_id AND cdfd3.team_id=tfd.id AND cdfd3.donation_frequency = 'onetime')", 'totalOneTime', [':campaign_id' => $campaign->id()]);
      $query->condition('campaign_id', $campaign->id(), '=');
      $query->condition('id', $entity->id(), '=');
      $result = $query->execute();
      $records = $result->fetchAll();
    }
  }

  return $value = $records[0]->totalOneTime;
}

/**
 * Computed field_monthly_donors.
 *
 * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
 *   The Entity Type Manager service.
 * @param \Drupal\campaign_kit_core\Entity\Team $entity
 *   The entity to be processed.
 * @param array $fields
 *   The available fields.
 * @param string $delta
 *   The index when computed field is being used more than once.
 *
 * @return string
 *   Returns $value.
 */
function computed_field_field_monthly_donors_compute($entity_type_manager, $entity, $fields, $delta) {
  $currentRoute = \Drupal::routeMatch();
  $campaign = $currentRoute->getParameters()->get('campaign');
  if ($campaign instanceof Campaign) {
    if ($entity instanceof Team) {
      $connection = \Drupal::getContainer()->get('database');
      $query = NULL;
      $query = $connection->select('team_field_data', 'tfd')->fields('tfd', ['id', 'name']);
      $query->addExpression("(SELECT count(*) FROM campaign_donation_field_data cdfd5 WHERE cdfd5.campaign_id = :campaign_id AND cdfd5.team_id=tfd.id AND cdfd5.donation_frequency = 'monthly')", 'totalMonthly', [':campaign_id' => $campaign->id()]);
      $query->condition('campaign_id', $campaign->id(), '=');
      $query->condition('id', $entity->id(), '=');
      $result = $query->execute();
      $records = $result->fetchAll();
    }
  }

  return $value = $records[0]->totalMonthly;
}

/**
 * Computed field_number_of_members.
 *
 * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
 *   The Entity Type Manager service.
 * @param \Drupal\campaign_kit_core\Entity\Team $entity
 *   The entity to be processed.
 * @param array $fields
 *   The available fields.
 * @param string $delta
 *   The index when computed field is being used more than once.
 *
 * @return string
 *   Returns $value.
 */
function computed_field_field_number_of_members_compute($entity_type_manager, $entity, $fields, $delta) {
  $currentRoute = \Drupal::routeMatch();
  $campaign = $currentRoute->getParameters()->get('campaign');
  if ($campaign instanceof Campaign) {
    if ($entity instanceof Team) {
      $connection = \Drupal::getContainer()->get('database');
      $query = NULL;
      $query = $connection->select('team_field_data', 'tfd')->fields('tfd', ['id', 'name']);
      $query->addExpression("(SELECT count(name) FROM campaign_donation_field_data cdfd6 WHERE cdfd6.campaign_id = :campaign_id AND cdfd6.team_id=tfd.id)", 'totalDonors', [':campaign_id' => $campaign->id()]);
      $query->condition('campaign_id', $campaign->id(), '=');
      $query->condition('id', $entity->id(), '=');
      $result = $query->execute();
      $records = $result->fetchAll();
    }
  }

  return $value = $records[0]->totalDonors;
}

/**
 * Computed field_last_donation.
 *
 * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
 *   The Entity Type Manager service.
 * @param \Drupal\campaign_kit_core\Entity\Team $entity
 *   The entity to be processed.
 * @param array $fields
 *   The available fields.
 * @param string $delta
 *   The index when computed field is being used more than once.
 *
 * @return string
 *   Returns $value.
 */
function computed_field_field_last_donation_compute($entity_type_manager, $entity, $fields, $delta) {
  $currentRoute = \Drupal::routeMatch();
  $campaign = $currentRoute->getParameters()->get('campaign');
  if ($campaign instanceof Campaign) {
    if ($entity instanceof Team) {
      $connection = \Drupal::getContainer()->get('database');
      $query = NULL;
      $query = $connection->select('team_field_data', 'tfd')->fields('tfd', ['id', 'name']);
      $query->addExpression("(SELECT max(created) FROM campaign_donation_field_data cdfd1 WHERE cdfd1.campaign_id = :campaign_id AND cdfd1.team_id=tfd.id)", 'lastDonationDate', [':campaign_id' => $campaign->id()]);
      $query->condition('campaign_id', $campaign->id(), '=');
      $query->condition('id', $entity->id(), '=');
      $result = $query->execute();
      $records = $result->fetchAll();
    }
  }

  if ($records[0]->lastDonationDate) {
    $dateFormatter = \Drupal::service('date.formatter');
    $lastDonationDate = $dateFormatter->format($records[0]->lastDonationDate, 'short');
  }
  else {
    $lastDonationDate = '';
  }

  return $value = $lastDonationDate;
}

/**
 * Implements hook_views_pre_view().
 */
function campaign_kit_core_views_pre_view($view, $display_id, array &$args) {
  $currentRoute = \Drupal::routeMatch();
  $campaign = $currentRoute->getParameters()->get('campaign');
  if ($view instanceof ViewExecutable) {
    if ($view->id() === 'leaderboard') {
      switch ($campaign->getFrequencyAllowed()) {
        case 'onetime':
          $view->removeHandler($display_id, 'field', 'field_team_total_one_time');
          $view->removeHandler($display_id, 'field', 'field_one_time_donors');
          break;

        case 'monthly':
          $view->removeHandler($display_id, 'field', 'field_team_total_monthly');
          $view->removeHandler($display_id, 'field', 'field_monthly_donors');
          break;
      }
    }
  }
}
