<?php

namespace Drupal\campaign_kit_core\ListBuilder;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Campaign entities.
 *
 * @ingroup campaign_kit_core
 */
class CampaignListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Campaign ID');
    $header['name'] = $this->t('Name');
    $header['total_one_time'] = $this->t('Total One-Time');
    $header['total_monthly'] = $this->t('Total Monthly');
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    // Get Currency code.
    $currencyCode = $this->getCurrencySymbol();

    /* @var $entity \Drupal\campaign_kit_core\Entity\Campaign */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'campaign_kit.campaign_kit_controller_view_campaign_entity',
      ['campaign' => $entity->id()]
    );
    $row['total_one_time'] = $currencyCode . $entity->getRaisedAmountOneTime();
    $row['total_monthly'] = $currencyCode . $entity->getRaisedAmountMonthly();
    $row['status'] = $entity->getCampaignStatus();
    return $row + parent::buildRow($entity);
  }

  /**
   * Gets the currency code.
   *
   * @return mixed
   *   Returns the Currency code.
   */
  private function getCurrencySymbol() {
    // TODO: Need central location for getCurrencySymbol().
    $currencyCode = '';
    // Get currency from plugin.
    $config = \Drupal::config('campaign_kit.settings');
    $clientPlugin = $config->get('campaign_kit');

    switch ($clientPlugin['currency']) {
      case 'USD':
        $currencyCode = '$';
        break;
    }

    return $currencyCode;
  }

}
