<?php

namespace Drupal\campaign_kit_core\ListBuilder;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Campaign donation entities.
 *
 * @ingroup campaign_kit_core
 */
class CampaignDonationListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['campaign_id'] = $this->t('Campaign ID');
    $header['campaign_name'] = $this->t('Campaign Name');
    $header['full_name'] = $this->t('Full Name');
    $header['frequency'] = $this->t('Frequency');
    $header['one_time_donation'] = $this->t('One-Time Donation');
    $header['monthly_donation'] = $this->t('Monthly Donation');
    $header['date_time'] = $this->t('Date');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\campaign_kit_core\Entity\CampaignDonation */
    $row['campaign_id'] = $entity->getCampaign()->id();
    $row['campaign_name'] = Link::createFromRoute(
      $entity->getCampaign()->label(),
      'campaign_kit.campaign_kit_controller_view_campaign_entity',
      ['campaign' => $entity->getCampaign()->id()]
    );
    $row['full_name'] = Link::createFromRoute(
      $entity->label(),
      'entity.campaign_donation.edit_form',
      ['campaign_donation' => $entity->id()]
    );

    $row['frequency'] = $this->getFrequencyLabel($entity->getField('donation_frequency'));

    $currencySymbol = $this->getCurrencySymbol($entity->getField('currency'));
    // Check if it is ONE-TIME or MONTHLY.
    if ($entity->getField('donation_frequency') == 'onetime') {
      $row['one_time_donation'] = $currencySymbol . $entity->getField('amount');
      $row['monthly_donation'] = '';
    }
    else {
      $row['one_time_donation'] = '';
      $row['monthly_donation'] = $currencySymbol . $entity->getField('amount');
    }

    $row['date_time'] = \Drupal::service('date.formatter')->format($entity->getCreatedTime(), 'short');
    return $row + parent::buildRow($entity);
  }

  /**
   * Gets the currency symbol.
   *
   * @param string $currencySymbol
   *   The currency code.
   *
   * @return string
   *   The currency symbol.
   */
  private function getCurrencySymbol($currencyCode) {
    // TODO: Need central location for getCurrencySymbol().
    $currencySymbol = '';
    switch ($currencyCode) {
      case 'USD':
        $currencySymbol = '$';
        break;

    }
    return $currencySymbol;
  }

  /**
   * Gets the Frequency label.
   *
   * @param string $frequency
   *   The frequency.
   *
   * @return string
   *   Returns the frequency label.
   */
  private function getFrequencyLabel($frequency) {
    switch ($frequency) {
      case 'onetime':
        $frequencyLabel = t('One-time');
        break;

      case 'monthly':
        $frequencyLabel = t('Monthly');
        break;
    }
    return $frequencyLabel;
  }

}
