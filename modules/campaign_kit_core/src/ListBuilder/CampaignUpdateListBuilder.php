<?php

namespace Drupal\campaign_kit_core\ListBuilder;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Campaign update entities.
 *
 * @ingroup campaign_kit_core
 */
class CampaignUpdateListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['campaign_id'] = $this->t('Campaign ID');
    $header['campaign_name'] = $this->t('Campaign Name');
    $header['date_time'] = $this->t('Date');
    $header['update_title'] = $this->t('Update title');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\campaign_kit_core\Entity\CampaignUpdate */
    $row['campaign_id'] = $entity->getCampaign()->id();
    $row['campaign_name'] = Link::createFromRoute(
      $entity->getCampaign()->label(),
      'campaign_kit.campaign_kit_controller_view_campaign_entity',
      ['campaign' => $entity->getCampaign()->id()]
    );
    $row['date_time'] = \Drupal::service('date.formatter')->format($entity->getCreatedTime(), 'short');
    $row['update_title'] = Link::createFromRoute(
      $entity->label(),
      'entity.campaign_update.edit_form',
      ['campaign_update' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
