<?php

namespace Drupal\campaign_kit_core\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Team entities.
 *
 * @ingroup campaign_kit_core
 */
class TeamDeleteForm extends ContentEntityDeleteForm {


}
