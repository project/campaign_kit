<?php

namespace Drupal\campaign_kit_core\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Campaign donation edit forms.
 *
 * @ingroup campaign_kit_core
 */
class CampaignDonationForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\campaign_kit_core\Entity\CampaignDonation */
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created the %label Campaign donation.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved the %label Campaign donation.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.campaign_donation.canonical', ['campaign_donation' => $entity->id()]);
  }

}
