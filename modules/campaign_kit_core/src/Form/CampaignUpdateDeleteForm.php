<?php

namespace Drupal\campaign_kit_core\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Campaign update entities.
 *
 * @ingroup campaign_kit_core
 */
class CampaignUpdateDeleteForm extends ContentEntityDeleteForm {


}
