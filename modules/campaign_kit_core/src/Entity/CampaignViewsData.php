<?php

namespace Drupal\campaign_kit_core\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Campaign entities.
 */
class CampaignViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    $this->attachDateTimeViewsData($data);

    return $data;
  }

  /**
   * @param $data
   */
  protected function attachDateTimeViewsData(&$data) {
    $datetime_columns = [
      'start_date',
      'end_date',
    ];

    foreach ($datetime_columns as $datetime_column_name) {
      $data['campaign_field_data'][$datetime_column_name]['filter']['id'] = 'datetime';
      $data['campaign_field_data'][$datetime_column_name]['filter']['field_name'] = $datetime_column_name;
      $data['campaign_field_data'][$datetime_column_name]['argument']['id'] = 'datetime';
      $data['campaign_field_data'][$datetime_column_name]['argument']['field_name'] = $datetime_column_name;
      $data['campaign_field_data'][$datetime_column_name]['sort']['id'] = 'datetime';
      $data['campaign_field_data'][$datetime_column_name]['sort']['field_name'] = $datetime_column_name;
    }
  }

}
