<?php

namespace Drupal\campaign_kit_core\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Campaign transaction type entity.
 *
 * @ConfigEntityType(
 *   id = "campaign_transaction_type",
 *   label = @Translation("Campaign transaction type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\campaign_kit_core\ListBuilder\CampaignTransactionTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\campaign_kit_core\Form\CampaignTransactionTypeForm",
 *       "edit" = "Drupal\campaign_kit_core\Form\CampaignTransactionTypeForm",
 *       "delete" = "Drupal\campaign_kit_core\Form\CampaignTransactionTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\campaign_kit_core\Routing\CampaignTransactionTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "campaign_transaction_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "campaign_transaction",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/campaign_transaction_type/{campaign_transaction_type}",
 *     "add-form" = "/admin/structure/campaign_transaction_type/add",
 *     "edit-form" = "/admin/structure/campaign_transaction_type/{campaign_transaction_type}/edit",
 *     "delete-form" = "/admin/structure/campaign_transaction_type/{campaign_transaction_type}/delete",
 *     "collection" = "/admin/campaign_kit/campaign_transaction_types"
 *   }
 * )
 */
class CampaignTransactionType extends ConfigEntityBundleBase implements CampaignTransactionTypeInterface {

  /**
   * The Campaign transaction type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Campaign transaction type label.
   *
   * @var string
   */
  protected $label;

}
