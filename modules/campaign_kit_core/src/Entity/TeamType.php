<?php

namespace Drupal\campaign_kit_core\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Team type entity.
 *
 * @ConfigEntityType(
 *   id = "team_type",
 *   label = @Translation("Team type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\campaign_kit_core\ListBuilder\TeamTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\campaign_kit_core\Form\TeamTypeForm",
 *       "edit" = "Drupal\campaign_kit_core\Form\TeamTypeForm",
 *       "delete" = "Drupal\campaign_kit_core\Form\TeamTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\campaign_kit_core\TeamTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "team_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "team",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *   },
 *   links = {
 *     "canonical" = "/admin/campaign_kit/team_type/{team_type}",
 *     "add-form" = "/admin/campaign_kit/team_type/add",
 *     "edit-form" = "/admin/campaign_kit/team_type/{team_type}/edit",
 *     "delete-form" = "/admin/campaign_kit/team_type/{team_type}/delete",
 *     "collection" = "/admin/campaign_kit/team_types"
 *   }
 * )
 */
class TeamType extends ConfigEntityBundleBase implements TeamTypeInterface {

  /**
   * The Team type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Team type label.
   *
   * @var string
   */
  protected $label;

}
