<?php

namespace Drupal\campaign_kit_core\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Donation entities.
 */
class DonationViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    $this->attachDateTimeViewsData($data);
    return $data;
  }


  /**
   * @param $data
   */
  protected function attachDateTimeViewsData(&$data) {
    // Automatic integration blocked behind https://www.drupal.org/node/2489476.
    $datetime_columns = [
      'date',
    ];

    foreach ($datetime_columns as $datetime_column_name) {
      $data['donation_field_data'][$datetime_column_name]['filter']['id'] = 'datetime';
      $data['donation_field_data'][$datetime_column_name]['filter']['field_name'] = $datetime_column_name;
      $data['donation_field_data'][$datetime_column_name]['argument']['id'] = 'datetime';
      $data['donation_field_data'][$datetime_column_name]['argument']['field_name'] = $datetime_column_name;
      $data['donation_field_data'][$datetime_column_name]['sort']['id'] = 'datetime';
      $data['donation_field_data'][$datetime_column_name]['sort']['field_name'] = $datetime_column_name;
    }

  }
}
