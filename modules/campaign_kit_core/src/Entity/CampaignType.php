<?php

namespace Drupal\campaign_kit_core\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Campaign type entity.
 *
 * @ConfigEntityType(
 *   id = "campaign_type",
 *   label = @Translation("Campaign type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\campaign_kit_core\ListBuilder\CampaignTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\campaign_kit_core\Form\CampaignTypeForm",
 *       "edit" = "Drupal\campaign_kit_core\Form\CampaignTypeForm",
 *       "delete" = "Drupal\campaign_kit_core\Form\CampaignTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\campaign_kit_core\Routing\CampaignTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "campaign_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "campaign",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/campaign_type/{campaign_type}",
 *     "edit-form" = "/admin/structure/campaign_type/{campaign_type}/edit",
 *     "delete-form" = "/admin/structure/campaign_type/{campaign_type}/delete",
 *     "collection" = "/admin/campaign_kit/campaign_types"
 *   }
 * )
 */
class CampaignType extends ConfigEntityBundleBase implements CampaignTypeInterface {

  /**
   * The Campaign type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Campaign type label.
   *
   * @var string
   */
  protected $label;

}
