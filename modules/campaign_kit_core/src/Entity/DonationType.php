<?php

namespace Drupal\campaign_kit_core\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Donation type entity.
 *
 * @ConfigEntityType(
 *   id = "donation_type",
 *   label = @Translation("Donation type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\campaign_kit_core\ListBuilder\DonationTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\campaign_kit_core\Form\DonationTypeForm",
 *       "edit" = "Drupal\campaign_kit_core\Form\DonationTypeForm",
 *       "delete" = "Drupal\campaign_kit_core\Form\DonationTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\campaign_kit_core\Routing\DonationTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "donation_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "donation",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/donation_type/{donation_type}",
 *     "add-form" = "/admin/structure/donation_type/add",
 *     "edit-form" = "/admin/structure/donation_type/{donation_type}/edit",
 *     "delete-form" = "/admin/structure/donation_type/{donation_type}/delete",
 *     "collection" = "/admin/campaign_kit/donation_types"
 *   }
 * )
 */
class DonationType extends ConfigEntityBundleBase implements DonationTypeInterface {

  /**
   * The Donation type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Donation type label.
   *
   * @var string
   */
  protected $label;

}
