<?php

namespace Drupal\campaign_kit_core\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Team type entities.
 */
interface TeamTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
