<?php

namespace Drupal\campaign_kit_core\Entity;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\UserInterface;

/**
 * Defines the Campaign entity.
 *
 * @ingroup campaign_kit_core
 *
 * @ContentEntityType(
 *   id = "campaign",
 *   label = @Translation("Campaign"),
 *   bundle_label = @Translation("Campaign type"),
 *   handlers = {
 *     "storage" = "Drupal\campaign_kit_core\Storage\CampaignStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\entity_extra\Controller\ViewsEntityListBuilder",
 *     "views_data" = "Drupal\campaign_kit_core\Entity\CampaignViewsData",
 *     "translation" = "Drupal\campaign_kit_core\Translation\CampaignTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\campaign_kit_core\Form\CampaignForm",
 *       "add" = "Drupal\campaign_kit_core\Form\CampaignForm",
 *       "edit" = "Drupal\campaign_kit_core\Form\CampaignForm",
 *       "delete" = "Drupal\campaign_kit_core\Form\CampaignDeleteForm",
 *     },
 *     "access" = "Drupal\campaign_kit_core\Access\CampaignAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\campaign_kit_core\Routing\CampaignHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "campaign",
 *   data_table = "campaign_field_data",
 *   revision_table = "campaign_revision",
 *   revision_data_table = "campaign_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer campaign entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log",
 *   },
 *   links = {
 *     "canonical" = "/campaign/{campaign}",
 *     "add-form" = "/campaign/add/{campaign_type}",
 *     "edit-form" = "/campaign/{campaign}/edit",
 *     "delete-form" = "/campaign/{campaign}/delete",
 *     "version-history" = "/campaign/{campaign}/revisions",
 *     "revision" = "/campaign/{campaign}/revisions/{campaign_revision}/view",
 *     "revision_revert" =
 *   "/campaign/{campaign}/revisions/{campaign_revision}/revert",
 *     "revision_delete" =
 *   "/campaign/{campaign}/revisions/{campaign_revision}/delete",
 *     "translation_revert" =
 *   "/campaign/{campaign}/revisions/{campaign_revision}/revert/{langcode}",
 *     "collection" = "/admin/campaign_kit/campaigns",
 *   },
 *   bundle_entity_type = "campaign_type",
 *   field_ui_base_route = "entity.campaign_type.edit_form"
 * )
 */
class Campaign extends RevisionableContentEntityBase implements CampaignInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the campaign owner
    // the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('title', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * Gets the Campaign title.
   *
   * @return mixed
   *   Returns the Campaign title.
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * Sets the Campaign title.
   *
   * @param string $name
   *   Receives the new Campaign name.
   *
   * @return $this
   */
  public function setTitle($name) {
    $this->set('title', $name);
    return $this;
  }

  /**
   * Sets the Campaign status.
   *
   * @param string $status
   *   Receives the new Campaign status.
   *
   * @return $this
   */
  public function setCampaignStatus($status) {
    $this->set('campaign_status', $status);
    return $this;
  }

  /**
   * Gets the Campaign ID.
   *
   * @return string
   *   Returns the ID.
   */
  public function getCampaignId() {
    return $this->get('id')->value;
  }

  /**
   * Gets the Campaign type.
   *
   * @return string
   *   Returns the type.
   */
  public function getCampaignType() {
    return $this->get('campaign_type')->value;
  }

  /**
   * Gets the Donate path.
   *
   * @return mixed
   *   Returns the path value.
   */
  public function getPath() {
    return $this->get('donate_path')->value;
  }

  /**
   * Gets goal amount one-time.
   *
   * @return int
   *   Returns an integer.
   */
  public function getGoal() {
    return (int) $this->get('one_time_donation_goal')->value;
  }

  /**
   * Custom Get goal amount monthly.
   *
   * @return int
   *   Returns an integer.
   */
  public function getGoalSustaining() {
    return (int) $this->get('monthly_donation_goal')->value;
  }

  /**
   * Gets the Start Date.
   */
  public function getStartDate() {
    return $this->get('start_date')->value;
  }

  /**
   * Gets the End Date.
   */
  public function getEndDate() {
    return $this->get('end_date')->value;
  }

  /**
   * Gets the Days Left for a Campaign.
   *
   * @return array
   *   Returns an array.
   */
  public function getDaysLeft() {
    // Actual date.
    /* \Drupal\Core\Datetime\DrupalDateTime */
    $datetimeStart = new DrupalDateTime();
    // Field_campaign_end_date field.
    /* \Drupal\Core\Datetime\DrupalDateTime */
    $datetimeEnd = new DrupalDateTime($this->get('end_date')->value);
    $interval = $datetimeStart->diff($datetimeEnd);

    // Returns $interval->format('%a').
    $positive = ($interval->format('%R') == '+') ? TRUE : FALSE;

    $daysLeft = (int) $interval->format('%a');
    if ($positive) {
      if ($daysLeft > 0) {
        $res = [
          'days' => $daysLeft,
          'message' => $this->getDaysLeftMessage($daysLeft),
        ];
      }
      else {
        $res = [
          'days' => $daysLeft,
          'message' => $this->getDaysLeftMessage($daysLeft),
        ];
      }
    }
    else {
      $daysLeftNegative = $daysLeft * (-1);
      $res = [
        'days' => $daysLeftNegative,
        'message' => $this->getDaysLeftMessage($daysLeftNegative),
      ];
    }

    return $res;
  }

  /**
   * Gets the Days Left message string.
   *
   * @param int $daysLeft
   *   Receives the daysLeft to validate.
   *
   * @return string
   *   Returns the message.
   */
  public function getDaysLeftMessage($daysLeft) {
    if ($daysLeft < 0) {
      $message = t('Campaign Ended');
    }
    elseif ($daysLeft == 0) {
      $message = t('Today finishes');
    }
    elseif ($daysLeft == 1) {
      $message = t('Day left');
    }
    else {
      $message = t('Days left');
    }
    return $message;
  }

  /**
   * Gets the Campaign Status.
   *
   * @return mixed
   *   Returns Campaign status.
   */
  public function getCampaignStatus() {
    return $this->get('campaign_status')->value;
  }

  /**
   * Gets Facebook path.
   */
  public function getFacebookPath() {
    return $this->get('facebook_url')->value;
  }

  /**
   * Gets Twitter path.
   */
  public function getTwitterPath() {
    return $this->get('twitter_url')->value;
  }

  /**
   * Gets Instagram path.
   */
  public function getInstagramPath() {
    return $this->get('instagram_url')->value;
  }

  /**
   * Gets the Campaign frequency allowed.
   */
  public function getFrequencyAllowed() {
    return $this->get('donation_frequency_allowed')->value;
  }

  /**
   * {@inheritdoc}
   *
   * @return int
   *   Return Parent ID.
   */
  public function getParent() {
    return $this->get('parent_id')->entity;
  }

  /**
   * Gets number of Supporters/Donors.
   */
  public function getNumberSupporters() {
    // TODO: getNumberSupportersMonthly function
    // Query number of supporters.
    $campaignDonations = \Drupal::entityQuery('campaign_donation')
      ->condition('campaign_id', $this->get('id')->value, '=')
      ->condition('payment_status', 'Confirmed', '=')
      // ->condition('anonymous', 0, '=').
      ->count()
      ->execute();
    return (int) $campaignDonations;
  }

  /**
   * Get percentage.
   *
   * @return int
   *   Returns percetage.
   */
  public function getPercentage() {
    // TODO: getPercentageMonthly function.
    $saved = $this->getRaisedAmount();
    $goal = $this->getGoal();
    $percentage = ($saved * 100) / $goal;
    return (int) $percentage;
  }

  /**
   * Gets the Raised amount.
   *
   * @return float|int
   *   Returns the amount.
   */
  public function getRaisedAmount() {
    // TODO: GET Campaign's 'saved' field of all Campaign Donation entities.
    $donationIds = \Drupal::entityQuery('campaign_donation')
      ->condition('campaign_id', $this->get('id')->value, '=')
      ->condition('payment_status', 'Confirmed', '=')
      ->execute();
    /* @var $entries \Drupal\campaign_kit_core\Entity\CampaignDonation */
    $entries = CampaignDonation::loadMultiple($donationIds);

    $raisedAmount = 0.0;
    foreach ($entries as $entry) {
      // Here we can get whatever we want of each Campaign Donation entity.
      $raisedAmount += (float) $entry->get('amount')->value;
    }
    return $raisedAmount;
  }

  /**
   * Gets the one-time Raised amount.
   *
   * @return float|int
   *   Returns the one-time amount.
   */
  public function getRaisedAmountOneTime() {
    // TODO: GET Campaign's 'saved' field of all Campaign Donation entities.
    $donationIds = \Drupal::entityQuery('campaign_donation')
      ->condition('campaign_id', $this->get('id')->value, '=')
      ->condition('payment_status', 'Confirmed', '=')
      ->condition('donation_frequency', 'onetime', '=')
      ->execute();
    /* @var $entries \Drupal\campaign_kit_core\Entity\CampaignDonation */
    $entries = CampaignDonation::loadMultiple($donationIds);

    $raisedAmount = 0.0;
    foreach ($entries as $entry) {
      // Here we can get whatever we want of each Campaign Donation entity.
      $raisedAmount += (float) $entry->get('amount')->value;
    }
    return $raisedAmount;
  }

  /**
   * Gets the monthly Raised amount.
   *
   * @return float|int
   *   Returns the monthly amount.
   */
  public function getRaisedAmountMonthly() {
    // TODO: GET Campaign's 'saved' field of all Campaign Donation entities.
    $donationIds = \Drupal::entityQuery('campaign_donation')
      ->condition('campaign_id', $this->get('id')->value, '=')
      ->condition('payment_status', 'Confirmed', '=')
      ->condition('donation_frequency', 'monthly', '=')
      ->execute();
    /* @var $entries \Drupal\campaign_kit_core\Entity\CampaignDonation */
    $entries = CampaignDonation::loadMultiple($donationIds);

    $raisedAmount = 0.0;
    foreach ($entries as $entry) {
      // Here we can get whatever we want of each Campaign Donation entity.
      $raisedAmount += (float) $entry->get('amount')->value;
    }
    return $raisedAmount;
  }

  /**
   * Gets the Owner name.
   *
   * @return string
   *   Returns the Owner name.
   */
  public function getOwnerName() {
    $fundraiser = $this->getOwner();
    if ($fundraiser->name->value != '') {
      $fundraiserName = $fundraiser->name->value;
    }
    else {
      $fundraiserName = "anonymous";
    }
    return $fundraiserName;
  }

  /**
   * Get boolean if a Campaign is open.
   *
   * @return bool
   *   Returns boolean if a Campaign is OPEN or not.
   */
  public function isCampaignOpen() {
    $donationsAllowed = TRUE;
    $campaignStillRunning = TRUE;

    if ($this->getCampaignStatus() !== 'open') return FALSE;

    switch ($this->getFrequencyAllowed()) {
      case 'onetime':
        if ($this->isPublished()) {
          // When a donation occurs that exceeds the value of the goals.
          if ($this->getRaisedAmountOneTime() > $this->get('one_time_donation_goal')->value && $this->get('campaign_success_type')->value === 'onetime'){
            //$this->setIsValid(FALSE);
            $donationsAllowed = FALSE;
          }

          // if current date passes Campaign end date.
          $currentDate = new DrupalDateTime();
          $datetimeEnd = new DrupalDateTime($this->get('end_date')->value);
          if ($currentDate->getTimestamp() > $datetimeEnd->getTimestamp()) {
            if ($this->get('use_campaign_end_date')->value) {
              $campaignStillRunning = FALSE;
            }
          }

          // Check both cases to define if a Campaign is open.
          if ($donationsAllowed == FALSE && $campaignStillRunning == FALSE) {
            return FALSE;
          }
          else {
            return TRUE;
          }
        }
        break;

      case 'monthly':
        if ($this->isPublished()) {
          // When a donation occurs that exceeds the value of the goals.
          if ($this->getRaisedAmountMonthly() > $this->get('monthly_donation_goal')->value && $this->get('campaign_success_type')->value === 'monthly'){
            $donationsAllowed = FALSE;
          }

          // if current date passes Campaign end date.
          $currentDate = new DrupalDateTime();
          $datetimeEnd = new DrupalDateTime($this->get('end_date')->value);
          if ($currentDate->getTimestamp() > $datetimeEnd->getTimestamp()) {
            if ($this->get('use_campaign_end_date')->value) {
              $campaignStillRunning = FALSE;
            }
          }

          // Check both cases to define if a Campaign is open.
          if ($donationsAllowed == FALSE && $campaignStillRunning == FALSE) {
            return FALSE;
          }
          else {
            return TRUE;
          }
        }
        break;

      case 'both':
        if ($this->isPublished()) {
          // When a donation occurs that exceeds the value of the goals.
          if ($this->getRaisedAmountMonthly() > $this->get('monthly_donation_goal')->value && $this->getRaisedAmountMonthly() > $this->get('monthly_donation_goal')->value && $this->get('campaign_success_type')->value === 'both'){
            $donationsAllowed = FALSE;
          }

          // if current date passes Campaign end date.
          $currentDate = new DrupalDateTime();
          $datetimeEnd = new DrupalDateTime($this->get('end_date')->value);
          if ($currentDate->getTimestamp() > $datetimeEnd->getTimestamp()) {
            if ($this->get('use_campaign_end_date')->value) {
              $campaignStillRunning = FALSE;
            }
          }

          // Check both cases to define if a Campaign is open.
          if ($donationsAllowed == FALSE && $campaignStillRunning == FALSE) {
            return FALSE;
          }
          else {
            return TRUE;
          }
        }
        break;

    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Campaign entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription('')
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 120,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => -23,
      ])
      ->setDisplayOptions('form', [
        'weight' => -23,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['campaign_type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Campaign type'))
      ->setDescription('')
      ->setRequired(TRUE)
      ->setSettings([
        'max_length' => 16,
        'text_processing' => 0,
        'allowed_values' => [
          'standalone' => 'Standalone',
          'parent' => 'Parent',
          'child' => 'Child',
        ],
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => -22,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -22,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['campaign_external_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Campaign external ID'))
      ->setDescription('')
      ->setRevisionable(TRUE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 120,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => -21,
      ])
      ->setDisplayOptions('form', [
        'weight' => -21,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['campaign_status'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Campaign status'))
      ->setDescription('')
      ->setRequired(TRUE)
      ->setSettings([
        'max_length' => 60,
        'text_processing' => 0,
        'allowed_values' => [
          'open' => 'Open',
          'archived' => 'Archived',
          'closed' => 'Closed',
        ],
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => -20,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['team_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Team ID'))
      ->setDescription('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => -19,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -19,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['parent_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Parent campaign'))
      ->setDescription('')
      ->setSetting('target_type', 'campaign')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => -18,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -18,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['start_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Start date'))
      ->setDescription('')
      ->setRequired(TRUE)
      ->setSettings([
        'datetime_type' => 'datetime'
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'datetime_default',
        'weight' => -17,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => -17,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['end_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('End date'))
      ->setDescription('')
      ->setRequired(TRUE)
      ->setSettings([
        'datetime_type' => 'datetime'
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'datetime_default',
        'weight' => -16,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => -16,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['short_description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Short description'))
      ->setDescription('')
      ->setRequired(TRUE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 200,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('form', [
        'weight' => -15,
      ])
      ->setDisplayOptions('view', [
        'weight' => -15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setDescription('')
      ->setRequired(TRUE)
      ->setSettings([
        'default_value' => '',
        'text_processing' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textfield',
        'weight' => -14,
        'settings' => [
          'rows' => 4,
        ],
      ])
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'weight' => -14,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['header_image'] = BaseFieldDefinition::create('image')
      ->setLabel(t('Header image'))
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'image',
        'weight' => -13,
      ])
      ->setDisplayOptions('form', [
        'type' => 'image_image',
        'weight' => -13,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['use_teams'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Use Teams'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => -12,
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => -12,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['confirmation_email'] = BaseFieldDefinition::create('email')
      ->setLabel(t('Confirmation email (admins)'))
      ->setDescription('')
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 120,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => -11,
      ])
      ->setDisplayOptions('form', [
        'weight' => -11,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['one_time_donation_goal_minimum'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Minimum one-time donation goal'))
      ->setDescription('')
      ->setDefaultValue(100)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => -10,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['monthly_donation_goal_minimum'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Minimum monthly donation goal'))
      ->setDescription('')
      ->setDefaultValue(100)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => -9,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -9,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['one_time_donation_goal_suggested'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Suggested one-time donation goal'))
      ->setDescription('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => -8,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -8,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['monthly_donation_goal_suggested'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Suggested monthly donation goal'))
      ->setDescription('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => -7,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -7,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['one_time_donation_goal'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Campaign one-time donation goal'))
      ->setDescription('')
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => -6,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['monthly_donation_goal'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Campaign monthly donation goal'))
      ->setDescription('')
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['total_one_time_amount'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('One-Time Amount'))
      ->setDescription('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['total_monthly_amount'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Monthly Amount'))
      ->setDescription('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['badge_image'] = BaseFieldDefinition::create('image')
      ->setLabel(t('Badge image'))
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'image',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'image_image',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['max_children'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Max. child campaigns'))
      ->setDescription('')
      ->setDefaultValue(100)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => -3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['thank_you_message'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Thank-you message'))
      ->setDescription('')
      ->setRequired(TRUE)
      ->setSettings([
        'default_value' => '',
        'text_processing' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textfield',
        'weight' => -2,
        'settings' => [
          'rows' => 4,
        ],
      ])
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['donate_path'] = BaseFieldDefinition::create('string')
      ->setLabel(t("Payment Processor Donation Path"))
      ->setDescription('')
      ->setSettings([
        'max_length' => 120,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => -1,
      ])
      ->setDisplayOptions('form', [
        'settings' => [
          'placeholder' => '',
        ],
        'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['facebook_url'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Facebook URL'))
      ->setDescription('')
      ->setSettings([
        'max_length' => 120,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'settings' => [
          'placeholder' => '',
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['twitter_url'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Twitter URL'))
      ->setDescription('')
      ->setSettings([
        'max_length' => 120,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => 1,
      ])
      ->setDisplayOptions('form', [
        'settings' => [
          'placeholder' => '',
        ],
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['instagram_url'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Instagram URL'))
      ->setDescription('')
      ->setSettings([
        'max_length' => 120,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => 2,
      ])
      ->setDisplayOptions('form', [
        'settings' => [
          'placeholder' => '',
        ],
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['donation_page_title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Donation page title'))
      ->setDescription('')
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 120,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => 3,
      ])
      ->setDisplayOptions('form', [
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['donation_page_description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Donation page description'))
      ->setDescription('')
      ->setSettings([
        'default_value' => '',
        'text_processing' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textfield',
        'weight' => 4,
        'settings' => [
          'rows' => 4,
        ],
      ])
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['donation_frequency_allowed'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Donation frequency allowed'))
      ->setDescription('')
      ->setRequired(TRUE)
      ->setSettings([
        'max_length' => 10,
        'text_processing' => 0,
        'allowed_values' => [
          'onetime' => t('One-time'),
          'monthly' => t('Monthly'),
          'both' => t('Both (One or the other)'),
        ],
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => 5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['campaign_success_type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Campaign is Successful When:'))
      ->setDescription('')
      ->setRequired(TRUE)
      ->setSettings([
        'max_length' => 10,
        'text_processing' => 0,
        'allowed_values' => [
          'onetime' => t('One-time Goal is Met'),
          'monthly' => t('Monthly Goal is Met'),
          'both' => t('One-time and Monthly Goals Met'),
        ],
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => 6,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['use_campaign_end_date'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Use Campaign End Date'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'weight' => 7,
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 7,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Campaign is published.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

  /**
   * Gets Url Parameters.
   *
   * @param string $rel
   *   The $rel variable.
   *
   * @return array
   *   Returns and array.
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);
    if ($rel === 'revision_revert' && $this instanceof RevisionableContentEntityBase) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableContentEntityBase) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    return $uri_route_parameters;
  }

}
