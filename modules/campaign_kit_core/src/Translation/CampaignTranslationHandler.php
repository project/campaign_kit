<?php

namespace Drupal\campaign_kit_core\Translation;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for Campaign.
 */
class CampaignTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
