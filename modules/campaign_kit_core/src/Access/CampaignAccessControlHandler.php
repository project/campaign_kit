<?php

namespace Drupal\campaign_kit_core\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Campaign entity.
 *
 * @see \Drupal\campaign_kit_core\Entity\Campaign.
 */
class CampaignAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\campaign_kit_core\Entity\CampaignInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished campaign entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published campaign entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit campaign entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete campaign entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add campaign entities');
  }

}
