<?php

namespace Drupal\campaign_kit_core\Controller;

use Drupal\campaign_kit_core\Entity\TeamInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Define TeamController.
 */
class TeamController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * TeamController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   The date formatter service.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, DateFormatter $dateFormatter, Renderer $renderer) {
    $this->entityTypeManager = $entityTypeManager;
    $this->dateFormatter = $dateFormatter;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @var \Symfony\Component\DependencyInjection\ContainerInterface */
    return new static(
      $container->get('entity_type.manager'),
      $container->get('date.formatter'),
      $container->get('renderer')
    );
  }

  /**
   * Displays a Team revision.
   *
   * @param int $team_revision
   *   The Team revision ID.
   *
   * @return array
   *   An array for rendering.
   */
  public function revisionShow($team_revision) {
    $team = $this->entityTypeManager->getStorage('team')->loadRevision($team_revision);
    $view_builder = $this->entityTypeManager->getViewBuilder('team');

    return $view_builder->view($team);
  }

  /**
   * Page title callback for a Team revision.
   *
   * @param int $team_revision
   *   The Team revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($team_revision) {
    $team = $this->entityTypeManager->getStorage('team')->loadRevision($team_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $team->label(),
      '%date' => $this->dateFormatter->format($team->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Team.
   *
   * @param \Drupal\campaign_kit_core\Entity\TeamInterface $team
   *   A Team object.
   *
   * @return array
   *   An array for rendering.
   */
  public function revisionOverview(TeamInterface $team) {
    $account = $this->currentUser();
    $langcode = $team->language()->getId();
    $langname = $team->language()->getName();
    $languages = $team->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $team_storage = $this->entityTypeManager->getStorage('team');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $team->label()]) : $this->t('Revisions for %title', ['%title' => $team->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all team revisions") || $account->hasPermission('administer team entities')));
    $delete_permission = (($account->hasPermission("delete all team revisions") || $account->hasPermission('administer team entities')));

    $rows = [];

    $vids = $team_storage->revisionIds($team);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\campaign_kit_core\TeamInterface $revision */
      $revision = $team_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $team->getRevisionId()) {
          $link = $this->linkGenerator->generate($date, new Url('entity.team.revision', ['team' => $team->id(), 'team_revision' => $vid]));
        }
        else {
          $link = $team->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.team.translation_revert', ['team' => $team->id(), 'team_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.team.revision_revert', ['team' => $team->id(), 'team_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.team.revision_delete', ['team' => $team->id(), 'team_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['team_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
