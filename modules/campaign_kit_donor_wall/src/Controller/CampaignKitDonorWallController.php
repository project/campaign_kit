<?php

namespace Drupal\campaign_kit_donor_wall\Controller;

use Drupal\campaign_kit_core\Entity\Campaign;
use Drupal\campaign_kit_core\Entity\Team;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityFormBuilderInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Link;
use Drupal\Core\Path\AliasManager;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Routing\RouteProvider;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CampaignKitDonorWallController.
 */
class CampaignKitDonorWallController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity form builder.
   *
   * @var \Drupal\Core\Entity\EntityFormBuilderInterface
   */
  protected $entityFormBuilder;

  /**
   * The route provider.
   *
   * @var \Drupal\Core\Routing\RouteProvider
   */
  protected $routeProvider;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The path alias manager.
   *
   * @var \Drupal\Core\Path\AliasManager
   */
  protected $pathAliasManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The account proxy interface.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The current path stack.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * The currently active request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The current active database's master connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The service date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * CampaignKitDonorWallController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFormBuilderInterface $entityFormBuilder
   *   The entity form builder.
   * @param \Drupal\Core\Routing\RouteProvider $routeProvider
   *   The route provider.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Path\AliasManager $pathAliasManager
   *   The path alias manager.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The account proxy interface.
   * @param \Drupal\Core\Path\CurrentPathStack $currentPath
   *   The current path stack.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The currently active request object.
   * @param \Drupal\Core\Database\Connection $connection
   *   The current active database's master connection.
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   The service date formatter.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, EntityFormBuilderInterface $entityFormBuilder, RouteProvider $routeProvider, ModuleHandlerInterface $moduleHandler, AliasManager $pathAliasManager, ConfigFactory $configFactory, AccountProxyInterface $currentUser, CurrentPathStack $currentPath, Request $request, Connection $connection, DateFormatter $dateFormatter) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFormBuilder = $entityFormBuilder;
    $this->routeProvider = $routeProvider;
    $this->moduleHandler = $moduleHandler;
    $this->pathAliasManager = $pathAliasManager;
    $this->configFactory = $configFactory;
    $this->currentUser = $currentUser;
    $this->currentPath = $currentPath;
    $this->request = $request;
    $this->connection = $connection;
    $this->dateFormatter = $dateFormatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @var \Symfony\Component\DependencyInjection\ContainerInterface */
    return new static(
        $container->get('entity_type.manager'),
        $container->get('entity.form_builder'),
        $container->get('router.route_provider'),
        $container->get('module_handler'),
        $container->get('path_alias.manager'),
        $container->get('config.factory'),
        $container->get('current_user'),
        $container->get('path.current'),
        $container->get('request_stack')->getCurrentRequest(),
        $container->get('database'),
        $container->get('date.formatter')
    );
  }

  /**
   * Donor page.
   *
   * @return array
   *   Return Template for Donor page.
   */
  public function donorWall(EntityInterface $campaign) {
    // Let's check if Campaign kit Donor Wall is enabled.
    $isItEnabled = $this->moduleHandler->moduleExists('campaign_kit_donor_wall');

    $renderDonor = views_embed_view('campaign_kit_donors', [
        'display_id' => 'block_1',
        'check_access' => TRUE,
    ]);

    $campaignTitle = $this->getCampaignTitle($campaign);

    $response = [
        '#theme' => 'campaign_kit_donor_wall',
        '#enable' => $isItEnabled,
        '#donor_wall' => $renderDonor,
        '#campaign_title' => $campaignTitle,
    ];

    return $response;
  }

  /**
   * Get Campaign title.
   *
   * @param \Drupal\Core\Entity\EntityInterface $campaign
   *   The Campaign entity.
   *
   * @return string
   *   Return the Campaign title.
   */
  public function getCampaignTitle(EntityInterface $campaign) {
    $campaignTitle = '';
    if ($campaign instanceof Campaign) {
      $campaignId = $campaign->id();
      // Check if the entity exists.
      $ids = $this->entityTypeManager->getStorage('campaign')->getQuery('AND')
          ->condition('id', $campaignId, '=')
          ->execute();

      if ($ids) {
        $campaignEntity = Campaign::load($campaignId);
        $campaignTitle = $campaignEntity->get('title')->value;
      }
    }
    return $campaignTitle;
  }

  /**
   * Donor Wall page.
   *
   * @return array
   *   Return Template for Donor Wall page.
   */
  public function donorWallPage() {
    // Let's check if Campaign kit Donor Wall module is enabled.
    $isItEnabled = $this->moduleHandler->moduleExists('campaign_kit_donor_wall');

    $renderDonorWall = views_embed_view('campaign_kit_donors', [
        'display_id' => 'block_2',
        'check_access' => TRUE,
    ]);

    $response = [
      '#theme' => 'campaign_kit_donor_wall',
      '#enable' => $isItEnabled,
      '#donor_wall' => $renderDonorWall,
    ];

    return $response;
  }

  /**
   * Gets the currency code.
   *
   * @param string $currency
   *   The Currency code to be validated.
   *
   * @return string
   *   The Currency Code.
   */
  private function getCurrency($currency) {
    $currencyCode = '';
    switch ($currency) {
      case 'USD':
        $currencyCode = '$';
        break;

    }
    return $currencyCode;
  }

}
