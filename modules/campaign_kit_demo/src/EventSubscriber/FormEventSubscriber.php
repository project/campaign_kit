<?php

namespace Drupal\campaign_kit_demo\EventSubscriber;

use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Define FormEventSubscriber.
 *
 * @package Drupal\campaign_kit_demo
 */
class FormEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[HookEventDispatcherInterface::FORM_ALTER][] = ['alterForm'];
    return $events;
  }

  /**
   * Alter form.
   *
   * @param \Drupal\hook_event_dispatcher\Event\Form\FormAlterEvent $event
   *   The event.
   */
  public function alterForm(FormAlterEvent $event) {
    $form = &$event->getForm();
    $formId = $event->getFormId();

    // Standalone Campaign form using standalone
    // form mode and without form mode.
    if ($formId == 'campaign_standalone_standalone_form' || $formId == 'campaign_standalone_edit_form') {
      $form['title']['widget'][0]['value']['#placeholder'] = t('Standalone campaign title');
    }
    // Standalone Campaign form to add a standalone campaign.
    if ($formId == 'campaign_standalone_add_form') {
      $form['title']['widget'][0]['value']['#placeholder'] = t('Standalone campaign title');
    }

    // Child Campaign form using child form mode to add and edit.
    if ($formId == 'campaign_child_child_form' || $formId == 'campaign_child_edit_form') {
      $form['title']['widget'][0]['value']['#placeholder'] = t('Child campaign title');
    }
    // Child Campaign default form to add child campaign.
    if ($formId == 'campaign_child_add_form') {
      $form['title']['widget'][0]['value']['#placeholder'] = t('Child campaign title');
    }

    // Parent Campaign form using parent form mode and without form mode.
    if ($formId == 'campaign_parent_parent_form' || $formId == 'campaign_parent_edit_form') {
      $form['title']['widget'][0]['value']['#placeholder'] = t('Parent campaign title');
    }
    // Parent Campaign default form to add parent campaign.
    if ($formId == 'campaign_parent_add_form') {
      $form['title']['widget'][0]['value']['#placeholder'] = t('Parent campaign title');
    }
  }

}
