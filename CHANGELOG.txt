Campaign Kit 2.0.0-beta2, 2020-09-30
------------------------------------
- many small bug fixes
- added fields to the Team view

Campaign Kit 2.0.0-beta1, 2020-08-21
------------------------------------
Changes since 1.0.0-beta1:
- switching to semantic versions i.e. 8.x-1.x --> 2.x
- badge component added in Campaign Standalone, Parent and Child page
- new filters in views for Campaign, Campaign Donation and Donation listing pages
- Drupal\Core\Path\AliasManager changed to Drupal\path_alias\AliasManager
- 'path.alias_manager' changed to 'path_alias.manager' service
- 'revision_metadata_keys' added to Campaign class entity for Drupal 9 compatibility
- 'config_export' added to CampaignType class entity for Drupal 9 compatibility
- 'revision_metadata_keys' added to CampaignDonation class entity for Drupal 9 compatibility
- 'config_export' added to CampaignDonationType class entity for Drupal 9 compatibility
- 'revision_metadata_keys' added to CampaignTransaction class entity for Drupal 9 compatibility
- 'config_export' added to CampaignTransactionType class entity for Drupal 9 compatibility
- 'revision_metadata_keys' added to Team class entity for Drupal 9 compatibility
- 'config_export' added to TeamType class entity for Drupal 9 compatibility
- 'revision_metadata_keys' added to Donation class entity for Drupal 9 compatibility
- 'config_export' added to DonationType class entity for Drupal 9 compatibility
- 'revision_metadata_keys' added to CampaignUpdate class entity for Drupal 9 compatibility
- 'config_export' added to CampaignUpdateType class entity for Drupal 9 compatibility
- hook_event_dispatcher 2.2
- form modes and view modes updated for Campaign Kit
- Total One-time and Monthly fields added to Campaign entity
- getViewsData method updated for DonationViewsData and CampaignViewsData classes
- drupalci.yml added
- drupal-check tool refactoring
- image extension and folder location fields added to Campaign Kit settings form form Single Donation
- frequency added for Single Donation
- Simple Thank You component added
- cron job removed
- CK Queuing dependency removed from CK Demo
- Badge component added
- changed Google Plus to Instagram

Campaign Kit 1.0.0-beta1, 2019-11-05
------------------------------------
Changes since 1.0.0-alpha7:
- reworded many internal code comments
- added beginnings of donor wall module
- upgraded deprecated drupal_set_message()
- supporters view updated

Campaign Kit 1.0.0-alpha7
Campaign Kit 1.0.0-alpha6, 2019-04-11
-------------------------------------
Changes since 1.0.0-alpha4:
- converted all list builders to views
- added Campaign Kit status check "pathauto is enabled"
- added Campaign Kit status check "payment module is enabled and at least one payment plugin is enabled"
- added Campaign Kit status check "Stripe keys are entered"
- hook_cron moved into an event with hook_event_dispatcher
- moved donation form logic to a service
- changed drupal/campaign_kit patch to drupal/payment
- added Donate component for Layout Builder pages
- added Thank You component
- moved css files for each component into a file inside their component directories
- moved Campaign page into a component
- converted Campaign totals on views to computed fields
- converted leaderboard into a view and used computed fields
- converted all node displays to Layout Builder

Campaign Kit 1.0.0-alpha5, 2019-04-11
-------------------------------------
- no release; skipped tag.

Campaign Kit 1.0.0-alpha4, 2019-02-06
-------------------------------------
Changes since 1.0.0-alpha3:
- moved Campaign Kit menu to the top level of Admin menu at the end
- cleaned up variable names, formatting, spelling across many files
- corrected the pathauto pattern creation bug
- now removes tables upon uninstall
- removed dead code
- changed name of DonationController to CampaignKitController
- added debug mode variable to configuration form
- files were moved to proper folders, e.g. Access, ListBuilder, Routing, Storage and Translation
- for the content demo, set the campaign end date to six months from installation time
- icon added for Campaign Kit menu top level
- added missing module dependencies
- translations were updated for Standalone, Parent and Child campaign templates
- add new teams module with more example data
- added leaderboard to campaigns e.g. campaign/new-campaign/leaderboard
- added new module Campaign Kit Queuing with a default queue and a default view
- added EntityQueue as a dependency of Campaign Kit Queuing
- added CampaignViewEventSubscriber class


Campaign Kit 1.0.0-alpha3, 2018-03-29
-------------------------------------
Changes since 1.0.0-alpha2:
- moved Payment Stripe code into its own module; there is still a dependency here we need to break in alpha4
- moved Campaign Kit Core and Campaign Kit Demo inside 'modules' directory inside Campaign Kit module
- created Campaign Kit Core to hold essential entities
- removed ck_subscriber module; moved remaining login into campaign_kit_demo
- tightened up deployment methodology including tests on current and prior versions of Drupal and OpenSocial
  (see the Developer Documentation | Release Procedure documentation page)
- many terminology changes
- changed admin/campaign_kit_page to admin/campaign_kit
- changed admin/structure/campaign_donation to admin/campaign_kit/campaign_donations
- changed admin/structure/campaign_update to admin/campaign_kit/campaign_updates
- changed admin/structure/campaign_transaction to admin/campaign_kit/campaign_transactions
- changed admin/structure/donation to admin/campaign_kit/donations
- changed admin/structure/campaign_type to admin/campaign_kit/campaign_types
- changed admin/structure/campaign_donation_type to admin/campaign_kit/campaign_donation_types
- changed admin/structure/campaign_transaction_type to admin/campaign_kit/campaign_transaction_types
- changed admin/structure/donation_type to admin/campaign_kit/donation_types
- changed campaign_kit/configuration/edit to admin/campaign_kit/configuration/edit
- changed campaign/badge to campaign/badges
- moved admin/campaign_kit out of campaign_kit_core

Views changed:
- changed CK-Badge → Campaign Kit Badges (campaign_kit_badges)
- changed CK-Campaign updates → Campaign Kit Updates (campaign_kit_updates)
- changed CK-Supporters → Campaign Kit Supporters (campaign_kit_supporters)

Menu items changed:
- changed Campaign list to Campaigns
- changed Campaign donation list to Campaign donations
- changed Campaign update list to Campaign updates
- changed Campaign transaction list to Campaign transactions
- changed Donation list to Donations
- changed Badge example to Badges


Campaign Kit 1.0.0-alpha2, 2018-02-09
-------------------------------------
- various changes.

Campaign Kit 1.0.0-alpha1, 2018-01-24
-------------------------------------
- initial release
