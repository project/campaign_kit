(function ($, Drupal) {
  Drupal.behaviors.ckCustom = {
    attach: function (context, settings) {

      // Copy URL to Clipboard.
      var clipboard = new Clipboard('.url-icon', {
        text: function() {
          return document.querySelector('#current-url').value;
        }
      });
      clipboard.on('success', function(e) {
        swal({
          title: 'Page Url has been copied!',
          type: 'success',
          showConfirmButton: false,
          timer: 2500,

        });
        e.clearSelection();
      });
      $("#current-url").val(location.href);
      // Safari.
      if (navigator.vendor.indexOf("Apple")==0 && /\sSafari\//.test(navigator.userAgent)) {
        $('.url-icon').on('click', function() {
          var msg = window.prompt("Copy this link", location.href);
        });
      }

      // Validate Dedicate option.
      $('#edit-dedicate-donation-1').change(function(){
        if($(this).is(':checked')) {
          document.getElementById('edit-dedication-information').style.display ='block';
        } else {
          document.getElementById('edit-dedication-information').style.display ='none';
        }
      });

      // Get donation amount of selected option.
      $('.form-radio').change(function(){
        var donations = document.getElementsByName('donation');

        for (var i = 0, length = donations.length; i < length; i++) {
          if (donations[i].checked && donations[i].value !== "other") {
            document.getElementById("edit-amount--2").value = donations[i].value;
            break;
          }
          if (donations[i].checked && donations[i].value === "other") {
            document.getElementById("edit-amount--2").value = "";
            break;
          }
        }
      });
    }
  };

  Drupal.behaviors.ckExposedForms = {
    attach: function (context, settings) {

      // Exposed forms.
      $('#edit-created').attr('size', '23');
      $('#edit-created-1').attr('size', '23');
      $('.reset-form').click(function(e){
        e.preventDefault();

        // Donation list.
        $('#edit-lowest-amount').val('');
        $('#edit-highest-amount').val('');
        $('#edit-frequency').val('');
        $('#edit-name').val('');


        // Campaign Donation list.
        $('#edit-created').val('');
        $('#edit-created-1').val('');
        $('#edit-donation-frequency').val('');
      });
    }
  };

  Drupal.behaviors.ckStateProvince = {
    attach: function (context, settings) {

      $(document).ready(function(){
        var countryKey = $( "#edit-country" ).val();
        searchCountryKey(countryKey)

        // On change for 'Country' select.
        $("#edit-country").change(function(){
          searchCountryKey(this.value)
        });
      });

      // Search CountryKey.
      function searchCountryKey(caountryKey) {
        switch(caountryKey) {
          case 'US':
            $("#us-output").removeClass("hide-state");
            $("#any-output").addClass("hide-state");
            $("#ca-output").addClass("hide-state");
            break;
          case 'CA':
            $("#us-output").addClass("hide-state");
            $("#ca-output").removeClass("hide-state");
            $("#any-output").addClass("hide-state");
            break;
          default:
            $("#us-output").addClass("hide-state");
            $("#ca-output").addClass("hide-state");
            $("#any-output").removeClass("hide-state");
        }
      }
    }
  };

})(jQuery, Drupal);
